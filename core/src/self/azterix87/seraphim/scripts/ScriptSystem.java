package self.azterix87.seraphim.scripts;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import self.azterix87.seraphim.GameContext;

public class ScriptSystem extends IteratingSystem {
    private GameContext gameContext;

    public ScriptSystem(GameContext gameContext, int priority) {
        super(Family.all(CScriptContainer.class).get(), priority);
        this.gameContext = gameContext;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        engine.addEntityListener(Family.all(CScriptContainer.class).get(), new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {

            }

            @Override
            public void entityRemoved(Entity entity) {
                CScriptContainer.mapper.get(entity).destroy(gameContext);
            }
        });
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CScriptContainer.mapper.get(entity).update(gameContext, deltaTime);
    }
}
