/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 4:47 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.scripts;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import self.azterix87.seraphim.GameContext;

import java.util.ArrayList;
import java.util.List;

public class CScriptContainer implements Component {
    public static ComponentMapper<CScriptContainer> mapper = ComponentMapper.getFor(CScriptContainer.class);
    private final List<Script> scripts = new ArrayList<>();
    private Entity entity;


    public CScriptContainer(Entity entity) {
        this.entity = entity;
    }

    public void update(GameContext gameContext, float dt) {
        for (Script script : scripts) {
            script.update(gameContext.getGameWorld(), dt);
        }
    }

    public void addScript(Script script) {
        scripts.add(script);
        try {
            script.link(this);
        } catch (ComponentAccessException e) {
            scripts.remove(script);
            Gdx.app.error("@ScriptContainer", "Error linking script " + script.getClass() + ": " + e.getMessage());
        }
    }

    public <T extends Component> T getComponent(ComponentMapper<T> mapper) throws ComponentAccessException {
        if (mapper.has(entity)) {
            return mapper.get(entity);
        } else {
            throw new ComponentAccessException("Requested component not found");
        }
    }

    public <T extends Script> T getScript(Class<T> scriptClass) throws ComponentAccessException {
        for (Script script : scripts) {
            if (script.getClass().equals(scriptClass)) {
                return (T) script;
            }
        }

        //Else
        throw new ComponentAccessException("Requested script not found");
    }

    public void destroy(GameContext gameContext) {
        for (Script script : scripts) {
            script.destroy(gameContext.getGameWorld());
        }
    }
}
