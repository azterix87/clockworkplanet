/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 4:50 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.scripts;

public class ComponentAccessException extends Exception {

    public ComponentAccessException() {

    }

    public ComponentAccessException(String message) {
        super(message);
    }
}
