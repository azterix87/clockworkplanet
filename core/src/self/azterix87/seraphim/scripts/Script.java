/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 4:47 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.scripts;

import self.azterix87.seraphim.GameWorld;

public abstract class Script {
    private boolean firstUpdate = true;

    public abstract void link(CScriptContainer scriptContainer) throws ComponentAccessException;

    public void firstUpdate(GameWorld gameWorld, float dt) {

    }

    public void update(GameWorld gameWorld, float dt) {
        if (firstUpdate) {
            firstUpdate(gameWorld, dt);
            firstUpdate = false;
        }
    }

    public abstract void destroy(GameWorld gameWorld);
}
