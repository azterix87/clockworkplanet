/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AssetCrawler {
    private List<AssetType> assetTypes = new ArrayList<>();


    public void registerAssetType(AssetType assetType) {
        assetTypes.add(assetType);
        Gdx.app.log("@AssetCrawler", "Registered asset type " + assetType.getName() + " " +
                Arrays.toString(assetType.getExtensions()));
    }

    public void loadAssets(FileHandle assetsFolder, AssetManager assetManager) {
        if (!assetsFolder.exists() || !assetsFolder.isDirectory()) {
            Gdx.app.error("@AssetCrawler", "Error: " + assetsFolder + " is not a valid directory");
            return;
        }

        Gdx.app.log("@AssetCrawler", "Searching for assets in /" + assetsFolder.path() + "/ ...");

        for (AssetType assetType : assetTypes) {
            if (assetType.hasSpecialLoader()) {
                assetManager.setLoader(assetType.getAssetClass(), assetType.getAssetLoader());
            }

            List<FileHandle> files = new ArrayList<>();
            for (String extension : assetType.getExtensions()) {
                getContentsRecursive(assetsFolder, extension, files);
            }

            for (FileHandle file : files) {
                if (assetType.hasParameters()) {
                    assetManager.load(file.path(), assetType.getAssetClass(), assetType.getParameters());
                } else {
                    assetManager.load(file.path(), assetType.getAssetClass());
                }
            }

            if (files.size() != 0) {
                Gdx.app.log("@AssetCrawler", "-- Found " + files.size() + " " + assetType.getName()
                        + (files.size() > 1 ? (assetType.getName().endsWith("s") ? "es --" : "s --") : " --"));
            }
        }

    }

    private void getContentsRecursive(FileHandle folder, String suffix, List<FileHandle> result) {
        for (FileHandle file : folder.list()) {
            if (file.isDirectory()) {
                getContentsRecursive(file, suffix, result);
            } else {
                if (file.name().toLowerCase().endsWith(suffix.toLowerCase())) {
                    result.add(file);
                }
            }
        }
    }
}

