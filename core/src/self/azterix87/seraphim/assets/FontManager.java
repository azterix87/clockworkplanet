/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class FontManager {
    private Assets assets;
    private List<String> reportedFonts = new ArrayList<>();
    private HashMap<String, BitmapFont> fontsMap = new HashMap<>();
    private BitmapFont defaultFont;

    public FontManager(Assets assets) {
        this.assets = assets;
        defaultFont = new BitmapFont();
    }

    public BitmapFont getFont(String fontName) {
        if (fontsMap.containsKey(fontName)) {
            return fontsMap.get(fontName);
        } else {
            if (!reportedFonts.contains(fontName)) {
                reportedFonts.add(fontName);
                Gdx.app.error("@FontManager", "Font not registered: \"" + fontName + "\", using default font");
            }
            return defaultFont;
        }
    }

    public void registerFont(String fontName, BitmapFont font) {
        fontsMap.put(fontName, font);
        Gdx.app.log("@FontManager", "Registered font \"" + fontName + "\"");
    }

    public void generateAndRegisterFont(String fontName, String fontGenerator, FreeTypeFontGenerator.FreeTypeFontParameter parameters) {
        FreeTypeFontGenerator generator = assets.getFontGenerator(fontGenerator);

        if (generator == null) {
            Gdx.app.error("@FontManager", "Error registering font \"" + fontName + "\", font generator not found");
            return;
        }

        BitmapFont font = generator.generateFont(parameters);
        registerFont(fontName, font);
    }

    public void dispose() {
        defaultFont.dispose();
        for (BitmapFont bitmapFont : fontsMap.values()) {
            bitmapFont.dispose();
        }
    }
}
