/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.assets;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.loaders.AssetLoader;

public class AssetType {
    private String name;
    private String[] extensions;
    private Class assetClass;
    private AssetLoader assetLoader;
    private AssetLoaderParameters parameters;

    public AssetType(String name, Class assetClass, String... extensions) {
        this.name = name;
        this.extensions = extensions;
        this.assetClass = assetClass;
    }

    public AssetType(String name, Class assetClass, AssetLoader assetLoader, String... extensions) {
        this.name = name;
        this.extensions = extensions;
        this.assetClass = assetClass;
        this.assetLoader = assetLoader;
    }

    public String getName() {
        return name;
    }

    public String[] getExtensions() {
        return extensions;
    }

    public Class getAssetClass() {
        return assetClass;
    }

    public AssetLoader getAssetLoader() {
        return assetLoader;
    }

    public void setAssetLoader(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
    }

    public AssetLoaderParameters getParameters() {
        return parameters;
    }

    public void setParameters(AssetLoaderParameters parameters) {
        this.parameters = parameters;
    }

    public boolean hasSpecialLoader() {
        return assetLoader != null;
    }

    public boolean hasParameters() {
        return parameters != null;
    }
}
