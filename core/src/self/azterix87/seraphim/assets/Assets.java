/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;
import self.azterix87.seraphim.systems.motionanimation.FramedMotionAnimation;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimation;

import java.util.ArrayList;
import java.util.List;

public class Assets {
    public final AssetManager assetManager;
    public final FontManager fontManager;
    protected List<String> notifiedPaths = new ArrayList<>();
    private Texture defaultTexture;
    private Animation defaultAnimation;
    private MotionAnimation defaultMotionAnimation;
    private TextureAtlas defaultTextureAtlas;

    public Assets(AssetManager assetManager) {
        this.assetManager = assetManager;
        this.fontManager = new FontManager(this);
    }

    public FreeTypeFontGenerator getFontGenerator(String path) {
        if (assetManager.isLoaded(path)) {
            return assetManager.get(path);
        } else {
            if (!notifiedPaths.contains(path)) {
                Gdx.app.error("@Assets", "Error font generator " + path + " not found");
                notifiedPaths.add(path);
            }
            return null;
        }
    }

    public BitmapFont getFont(String fontName) {
        return fontManager.getFont(fontName);
    }

    public MotionAnimation getMotionAnimation(String path) {
        if (assetManager.isLoaded(path)) {
            return assetManager.get(path);
        } else {
            if (!notifiedPaths.contains(path)) {
                Gdx.app.error("@Assets", "Error motionanimation " + path + " not found, using empty animation");
                notifiedPaths.add(path);
            }
            return getDefaultMotionAnimation();
        }
    }

    public Animation getAnimation(String path) {
        if (assetManager.isLoaded(path)) {
            return assetManager.get(path);
        } else {
            if (!notifiedPaths.contains(path)) {
                Gdx.app.error("@Assets", "Error animation " + path + " not found. Using empty animation");
                notifiedPaths.add(path);
            }
            return getDefaultAnimation();
        }
    }

    public Texture getTexture(String path) {
        if (assetManager.isLoaded(path)) {
            return assetManager.get(path);
        } else {
            if (!notifiedPaths.contains(path)) {
                Gdx.app.error("@Assets", "Error texture " + path + " not found. Using default texture");
                notifiedPaths.add(path);
            }
            return getDefaultTexture();
        }
    }

    public Texture getDefaultTexture() {
        if (defaultTexture == null) {
            defaultTexture = createDefaultTexture();
        }

        return defaultTexture;
    }

    public TextureAtlas getTextureAtlas(String path) {
        if (assetManager.isLoaded(path)) {
            return assetManager.get(path);
        } else {
            if (!notifiedPaths.contains(path)) {
                Gdx.app.error("@Assets", "Error texture atlas " + path + " not found, using empty atlas");
                notifiedPaths.add(path);
            }
            return getDefaultTextureAtlas();
        }
    }

    protected Animation getDefaultAnimation() {
        if (defaultAnimation == null) {
            Array<TextureRegion> frames = new Array<>();
            frames.add(new TextureRegion(getDefaultTexture()));
            defaultAnimation = new Animation(0, frames, Animation.PlayMode.LOOP);
        }

        return defaultAnimation;
    }

    protected MotionAnimation getDefaultMotionAnimation() {
        if (defaultMotionAnimation == null) {
            defaultMotionAnimation = new FramedMotionAnimation(new float[][]{}, 0, false);
        }
        return defaultMotionAnimation;
    }

    protected TextureAtlas getDefaultTextureAtlas() {
        if (defaultTextureAtlas == null) {
            defaultTextureAtlas = new TextureAtlas((TextureAtlas.TextureAtlasData) null);
        }
        return defaultTextureAtlas;
    }

    private Texture createDefaultTexture() {
        Gdx.app.log("@GameContext", "Creating default texture 32x32");
        Pixmap pixmap = new Pixmap(32, 32, Pixmap.Format.RGBA8888);

        pixmap.setColor(Color.PURPLE);
        pixmap.fillRectangle(0, 0, 32, 32);
        pixmap.setColor(Color.BLACK);
        pixmap.fillRectangle(0, 0, 16, 16);
        pixmap.fillRectangle(16, 16, 16, 16);
        return new Texture(pixmap);
    }

    public <T> T getAsset(String path, Class<T> type) {
        if (assetManager.isLoaded(path)) {
            return assetManager.get(path, type);
        } else {
            return null;
        }
    }

    public void dispose() {
        assetManager.dispose();
        fontManager.dispose();
    }

}
