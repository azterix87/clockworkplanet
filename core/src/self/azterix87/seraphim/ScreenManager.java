/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ScreenManager {
    private Game game;
    private Map<String, Screen> screens = new HashMap<>();
    private String currentScreen;
    private Stack<String> history = new Stack<>();

    public ScreenManager(Game game) {
        this.game = game;
    }

    public void addScreen(String name, Screen screen) {
        screens.put(name, screen);
    }

    public Screen getScreen(String name) {
        return screens.get(name);
    }

    public void removeScreen(String name) {
        screens.remove(name);
    }

    public String getCurrentScreen() {
        return currentScreen;
    }

    public void setScreen(String screen) {
        setScreen(screen, true);
    }

    public void setScreen(String screen, boolean saveInHistory) {
        if (screens.containsKey(screen)) {
            if (saveInHistory) {
                if (currentScreen != null) {
                    history.push(currentScreen);
                }
            }

            currentScreen = screen;
            game.setScreen(getScreen(screen));
        } else {
            Gdx.app.error("@ScreenManager", "Error, screen " + screen + " was not found");
        }

    }

    public void previousScreen() {
        if (!history.isEmpty()) {
            setScreen(history.pop(), false);
        } else {
            Gdx.app.error("@ScreenManager", "Error, no more screens in history");
        }
    }

    public void dispose() {
        for (Screen screen : screens.values()) {
            screen.dispose();
        }
    }

}
