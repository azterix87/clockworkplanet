package self.azterix87.seraphim;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.components.CTags;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class GameWorld {
    protected Engine entityEngine;
    protected GameContext gameContext;
    private final ImmutableArray<Entity> taggedEntities;

    public GameWorld(GameContext gameContext) {
        this.gameContext = gameContext;
        this.entityEngine = new Engine();
        taggedEntities = entityEngine.getEntitiesFor(Family.all(CTags.class).get());
    }

    public void update(float delta) {
        entityEngine.update(delta);
    }

    //Spawn functions

    public Entity spawnEntity(String entityType, float x, float y, float z, Map<String, String> parameters) {
        return spawnEntity(entityType, new Vector3(x, y, z), parameters);
    }


    public Entity spawnEntity(String entityType, Vector3 position, Map<String, String> parameters) {
        Entity e = gameContext.factoryManager.createEntity(entityType, position, parameters);
        if (e != null) {
            entityEngine.addEntity(e);
            Gdx.app.debug("@GameContext", "Entity [" + entityType + "] spawned at (" + position.x + ", " + position.y + ")");
        }
        return e;
    }


    public void addEntity(Entity entity) {
        entityEngine.addEntity(entity);
    }

    public void removeEntity(Entity entity) {
        entityEngine.removeEntity(entity);
    }

    public Entity findEntityWithTag(String tag) {
        List<Entity> entities = new ArrayList<>();
        for (Entity e : taggedEntities) {
            if (CTags.mapper.get(e).hasTag(tag)) {
                return e;
            }
        }
        return null;
    }

    public Entity[] findEntitiesWithTag(String tag) {
        List<Entity> entities = new ArrayList<>();
        for (Entity e : taggedEntities) {
            if (CTags.mapper.get(e).hasTag(tag)) {
                entities.add(e);
            }
        }

        return entities.toArray(new Entity[entities.size()]);
    }

    public void init() {
        initSystems();
        initFactories();
    }

    protected abstract void initFactories();

    protected abstract void initSystems();
}
