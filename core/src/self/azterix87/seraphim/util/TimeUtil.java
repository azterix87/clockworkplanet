/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.util;

public class TimeUtil {
    public static float seconds() {
        return (float) (System.nanoTime() / 1_000_000_000D);
    }
}
