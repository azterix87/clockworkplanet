/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector3;

import java.util.Map;

public interface ParametricEntityFactory {
    Entity createEntity(String type, Vector3 position, Map<String, String> parameters);

    String[] getSupportedEntityTypes();
}
