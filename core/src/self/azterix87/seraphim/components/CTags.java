/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.components;


import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

import java.util.HashSet;
import java.util.Set;

public class CTags implements Component {
    public static final ComponentMapper<CTags> mapper = ComponentMapper.getFor(CTags.class);
    private Set<String> tags = new HashSet<>();

    public void addTag(String tag) {
        tags.add(tag);
    }

    public void removeTag(String tag) {
        tags.remove(tag);
    }

    public boolean hasTag(String tag) {
        return tags.contains(tag);
    }

}
