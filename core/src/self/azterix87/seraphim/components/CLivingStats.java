/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;

import java.util.ArrayList;
import java.util.List;

public class CLivingStats implements Component {
    public static final ComponentMapper<CLivingStats> mapper = ComponentMapper.getFor(CLivingStats.class);
    private List<LivingStatsListener> listeners = new ArrayList<>();
    private float maxHealth;
    private float health;
    private boolean dead;

    public CLivingStats(float maxHealth) {
        this.maxHealth = maxHealth;
        this.health = maxHealth;
    }

    public void damage(float amount) {
        health = Math.max(0, health - amount);
        onDamaged(amount);

        if (health == 0) {
            dead = true;
            onDied();
        }
    }

    public void heal(float amount) {
        health = Math.min(maxHealth, health + amount);
        onHealed(amount);
    }

    public float getHealth() {
        return health;
    }

    public boolean isDead() {
        return dead;
    }

    public void addListener(LivingStatsListener listener) {
        listeners.add(listener);
    }

    public boolean removeListener(LivingStatsListener listener) {
        return listeners.remove(listener);
    }

    private void onDamaged(float amount) {
        for (LivingStatsListener listener : listeners) {
            listener.onEntityDamaged(amount, this);
        }
    }

    private void onHealed(float amount) {
        for (LivingStatsListener listener : listeners) {
            listener.onEntityHealed(amount, this);
        }
    }

    private void onDied() {
        for (LivingStatsListener listener : listeners) {
            listener.onEntityDied(this);
        }
    }
}
