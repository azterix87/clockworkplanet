/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.components;

public interface LivingStatsListener {
    void onEntityDamaged(float amount, CLivingStats livingStats);

    void onEntityHealed(float amount, CLivingStats livingStats);

    void onEntityDied(CLivingStats livingStats);
}
