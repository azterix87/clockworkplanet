/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class CTransform implements Component {
    public static final ComponentMapper<CTransform> mapper = ComponentMapper.getFor(CTransform.class);
    public final Vector3 localPosition = new Vector3();
    public final Vector2 scale = new Vector2(1, 1);
    public float rotation;
    private CTransform parent;

    public CTransform() {

    }

    public CTransform getParent() {
        return parent;
    }

    public void setParent(CTransform parent) {
        this.parent = parent;
    }

    public Vector3 getGlobalPosition() {
        if (parent != null) {
            return localPosition.cpy().mul(parent.getTransformMatrix());
        } else {
            return localPosition;
        }
    }

    public Matrix4 getTransformMatrix() {
        if (parent != null) {
            Matrix4 parentMatrix = parent.getTransformMatrix();
            parentMatrix.mul(getLocalTransformMatrix());
            return parentMatrix;
        } else {
            return getLocalTransformMatrix();
        }
    }

    public Matrix4 getLocalTransformMatrix() {
        Matrix4 matrix = new Matrix4();
        matrix.translate(localPosition);
        matrix.rotate(0, 0, 1, rotation);
        matrix.scale(scale.x, scale.y, 1);
        return matrix;
    }
}
