/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;

import java.util.HashMap;
import java.util.Map;

public class FactoryManager {
    private Map<String, ParametricEntityFactory> factories = new HashMap<>();

    public void registerFactory(ParametricEntityFactory factory) {
        for (String entityType : factory.getSupportedEntityTypes()) {
            if (factories.containsKey(entityType)) {
                Gdx.app.log("@FactoryManager", "Overriding factory of [" + entityType + "] by " + factory.getClass().getName());
            } else {
                Gdx.app.log("@FactoryManager", "Registering factory of [" + entityType + "] as " + factory.getClass().getName());
            }
            factories.put(entityType, factory);
        }
    }

    public Entity createEntity(String entityType, Vector3 position, Map<String, String> parameters) {
        if (!factories.containsKey(entityType)) {
            Gdx.app.error("@FactoryManager", "Unknown entity type [" + entityType + "]");
            return null;
        }

        ParametricEntityFactory factory = factories.get(entityType);
        return factory.createEntity(entityType, position, parameters);
    }
}
