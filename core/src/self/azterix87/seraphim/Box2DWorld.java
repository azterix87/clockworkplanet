package self.azterix87.seraphim;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import self.azterix87.seraphim.systems.box2d.Box2DSystem;

import java.util.ArrayList;
import java.util.List;

public abstract class Box2DWorld extends GameWorld {
    protected Box2DSystem physicsSystem;

    public Box2DWorld(GameContext gameContext) {
        super(gameContext);
    }

    @Override
    protected void initSystems() {
        physicsSystem = new Box2DSystem(0);
        entityEngine.addSystem(physicsSystem);
    }

    public Entity raytraceClosest(Vector2 from, Vector2 ray) {
        World world = getBox2dWorld();
        final Entity[] result = {null};
        final float[] smallestFraction = {Float.MAX_VALUE};

        RayCastCallback castCallback = (fixture, point, normal, fraction) -> {
            Body body = fixture.getBody();

            if (body.getUserData() != null && body.getUserData() instanceof Entity) {
                if (fraction < smallestFraction[0]) {
                    result[0] = (Entity) body.getUserData();
                    smallestFraction[0] = fraction;
                }
            }

            return 1;
        };

        world.rayCast(castCallback, from, from.cpy().add(ray));
        return result[0];
    }

    public List<Entity> raytrace(Vector2 from, Vector2 to) {
        World world = getBox2dWorld();
        List<Entity> foundEntities = new ArrayList<>();
        RayCastCallback castCallback = (fixture, point, normal, fraction) -> {
            Body body = fixture.getBody();

            if (body.getUserData() != null && body.getUserData() instanceof Entity) {
                foundEntities.add((Entity) body.getUserData());
            }

            return 1;
        };

        world.rayCast(castCallback, from, to);

        return foundEntities;
    }

    public World getBox2dWorld() {
        return physicsSystem.getWorld();
    }
}
