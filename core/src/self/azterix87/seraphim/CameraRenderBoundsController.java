/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:09 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim;

import com.badlogic.gdx.graphics.OrthographicCamera;
import self.azterix87.seraphim.systems.RenderArea;

public class CameraRenderBoundsController implements Controller {
    private final RenderArea renderArea;
    private float minElevation;
    private float maxElevation;
    private float border;

    public CameraRenderBoundsController(RenderArea renderArea) {
        this.renderArea = renderArea;
        this.minElevation = renderArea.getZ();
        this.maxElevation = renderArea.getZ() + renderArea.getElevation();
    }

    @Override
    public void update(GameContext gameContext, float delta) {
        OrthographicCamera camera = gameContext.getCurrentCamera();

        if (camera == null) {
            return; //Nothing to do here..
        }

        float camX = camera.position.x;
        float camY = camera.position.y;

        float width = camera.viewportWidth * camera.zoom + border * 2;
        float height = camera.viewportHeight * camera.zoom + border * 2;

        renderArea.setPosition(camX - width / 2, camY - height / 2, minElevation);
        renderArea.setBounds(width, height, maxElevation - minElevation);
    }

    public float getMinElevation() {
        return minElevation;
    }

    public void setMinElevation(float minElevation) {
        this.minElevation = minElevation;
    }

    public float getMaxElevation() {
        return maxElevation;
    }

    public void setMaxElevation(float maxElevation) {
        this.maxElevation = maxElevation;
    }

    public float getBorder() {
        return border;
    }

    public void setBorder(float border) {
        this.border = border;
    }
}
