/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.gui;

import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

public class BasicLayout implements Layout {
    private WidgetGroup root;

    public BasicLayout(WidgetGroup root) {
        this.root = root;
    }

    @Override
    public WidgetGroup getRoot() {
        return root;
    }
}
