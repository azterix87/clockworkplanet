/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.gui;

import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

public interface Layout {
    WidgetGroup getRoot();
}
