/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.gui;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class GUI {
    private Stage stage;
    private Layout layout;

    public GUI() {
        stage = new Stage(new ScreenViewport());
    }

    public void render() {
        stage.act();
        stage.draw();
    }

    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    public void dispose() {
        stage.dispose();
    }

    public InputProcessor getInputProcessor() {
        return stage;
    }

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        if (this.layout != null) {
            stage.clear();
        }
        this.layout = layout;
        stage.addActor(layout.getRoot());
    }
}
