/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public abstract class GUIScreen implements Screen {
    protected GUI gui;

    public GUIScreen() {
        gui = new GUI();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gui.render();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(gui.getInputProcessor());
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }


    @Override
    public void resize(int width, int height) {
        gui.resize(width, height);
    }

    @Override
    public void dispose() {
        gui.dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }
}
