package self.azterix87.seraphim.tiled;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import self.azterix87.seraphim.GameWorld;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.systems.box2d.CBox2DBody;

public class TiledHelper {
    public static void generateCollisionBoxes(TiledMap tiledMap, GameWorld gameWorld) {
        TiledMapTileLayer layer = (TiledMapTileLayer) tiledMap.getLayers().get("Walls");

        if (layer == null) {
            return;
        }

        PolygonShape box = new PolygonShape();
        box.setAsBox(0.5F, 0.5F);

        for(int x = 0; x < layer.getWidth(); x++) {
            for(int y = 0; y < layer.getHeight(); y++) {
                if(layer.getCell(x, y) != null) {
                    Entity entity = new Entity();
                    CTransform transform = new CTransform();
                    transform.localPosition.x = x + 0.5F;
                    transform.localPosition.y = y + 0.5F;
                    entity.add(transform);

                    BodyDef bodyDef = new BodyDef();
                    bodyDef.type = BodyDef.BodyType.StaticBody;
                    CBox2DBody bodyComponent = new CBox2DBody(bodyDef);

                    FixtureDef fixtureDef = new FixtureDef();
                    fixtureDef.shape = box;
                    bodyComponent.addFixtureDef(fixtureDef);

                    entity.add(bodyComponent);

                    gameWorld.addEntity(entity);
                }
            }
        }
    }
}
