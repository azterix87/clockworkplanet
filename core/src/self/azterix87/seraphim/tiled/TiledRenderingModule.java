package self.azterix87.seraphim.tiled;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.systems.RenderArea;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.RenderingSystem;

public class TiledRenderingModule implements RenderingModule {
    private OrthogonalTiledMapRenderer renderer;
    private RenderingSystem renderingSystem;

    @Override
    public void addedToRenderingSystem(RenderingSystem renderingSystem) {
        this.renderingSystem = renderingSystem;
    }

    @Override
    public void render(RenderArea renderArea, GameContext gameContext, float dt) {
        if(renderer != null) {
            renderer.setView(gameContext.getCurrentCamera());
            renderer.render();
        }
    }

    public void setMap(TiledMap tiledmap) {
        renderer = new OrthogonalTiledMapRenderer(tiledmap, 1 / renderingSystem.getPPM());
    }
}
