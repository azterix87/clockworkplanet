/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/9/17 7:45 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.event;

public interface GameEventListener {
    void onGameEvent(GameEvent gameEvent);
}
