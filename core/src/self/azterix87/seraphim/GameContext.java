/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:09 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import self.azterix87.seraphim.assets.Assets;

import java.util.ArrayList;
import java.util.List;

public class GameContext {
    public final SpriteBatch spriteBatch;
    public final FactoryManager factoryManager;
    public final Assets assets;
    private GameWorld gameWorld;
    private OrthographicCamera currentCamera;


    private final InputMultiplexer input;
    private final List<Controller> controllers = new ArrayList<>();

    public GameContext() {
        this.assets = new Assets(new AssetManager(new LocalFileHandleResolver()));
        this.spriteBatch = new SpriteBatch();
        this.factoryManager = new FactoryManager();
        this.input = new InputMultiplexer();
    }

    //Update
    public void update(float delta) {
        if (gameWorld != null) {
            gameWorld.update(delta);
        }

        for (Controller controller : controllers) {
            controller.update(this, delta);
        }
    }

    //Scripts and input processors

    public InputProcessor getInputProcessor() {
        return input;
    }

    public void addInputProcessor(InputProcessor processor) {
        input.addProcessor(processor);
    }

    public void addInputProcessor(int index, InputProcessor processor) {
        input.addProcessor(index, processor);
    }

    public void removeInputProcessor(InputProcessor processor) {
        input.removeProcessor(processor);
    }

    public void addController(Controller controller) {
        controllers.add(controller);
        addInputProcessor(controller);
        controller.initialize(this);
    }

    public void removeController(Controller controller) {
        controller.shutdown(this);
        removeInputProcessor(controller);
        controllers.remove(controller);
    }

    public GameWorld getGameWorld() {
        return gameWorld;
    }

    public void setGameWorld(GameWorld gameWorld) {
        this.gameWorld = gameWorld;
    }

    public OrthographicCamera getCurrentCamera() {
        return currentCamera;
    }

    public void setCurrentCamera(OrthographicCamera currentCamera) {
        this.currentCamera = currentCamera;
    }

    public void dispose() {
        spriteBatch.dispose();
        assets.dispose();
    }


}
