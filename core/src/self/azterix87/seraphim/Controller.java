/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 12:40 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim;

import com.badlogic.gdx.InputProcessor;
import self.azterix87.seraphim.event.GameEvent;
import self.azterix87.seraphim.event.GameEventListener;

public interface Controller extends InputProcessor, GameEventListener {

    default void initialize(GameContext gameContext) {
    }

    default void update(GameContext gameContext, float delta) {
    }

    default void shutdown(GameContext gameContext) {
    }

    @Override
    default void onGameEvent(GameEvent gameEvent) {
    }

    @Override
    default boolean keyDown(int keycode) {
        return false;
    }

    @Override
    default boolean keyUp(int keycode) {
        return false;
    }

    @Override
    default boolean keyTyped(char character) {
        return false;
    }

    @Override
    default boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    default boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    default boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    default boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    default boolean scrolled(int amount) {
        return false;
    }

}
