/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:42 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems;

public class RenderArea {
    private float x;
    private float y;
    private float z;
    private float width;
    private float height;
    private float elevation;

    private int currentSlice;
    private float slicingStep = -1;


    public RenderArea(float width, float height, float elevation) {
        setBounds(width, height, elevation);
    }

    public boolean nextSlice() {
        int sliceNumber = isSliced() ? sliceNumber() : 1;

        if (currentSlice < sliceNumber - 1) {
            currentSlice++;
            return true;
        } else {
            currentSlice = -1;
            return false;
        }

    }

    private float firsSliceY() {
        assert slicingStep > 0;
        return (float) Math.ceil((y + height) / slicingStep - 1) * slicingStep;
    }

    private float lastSliceY() {
        assert slicingStep > 0;
        return (float) Math.floor(y / slicingStep) * slicingStep;
    }

    public float currentSliceY() {
        return firsSliceY() - currentSlice * slicingStep;
    }

    private int sliceNumber() {
        return (int) ((firsSliceY() - lastSliceY()) / slicingStep + 1);
    }

    private boolean isSliced() {
        return slicingStep > 0;
    }


    private boolean isInBounds(float x, float y, float z) {
        return x >= this.x && x <= this.x + width &&
                y >= this.y && y <= this.y + height &&
                z >= this.z && z <= this.z + height;
    }

    public boolean isInSliceBounds(float x, float y, float z) {
        //If object is not in general bounds it can't be in slice bounds
        if (!isInBounds(x, y, z)) {
            return false;
        }

        //Assuming entity is inside bounds
        if (isSliced()) {
            float curY = currentSliceY();
            return y > curY && y <= curY + slicingStep;
        } else {
            return true;
        }
    }

    public void setPosition(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void setBounds(float width, float height, float elevation) {
        this.width = width;
        this.height = height;
        this.elevation = elevation;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public float getSlicingStep() {
        return slicingStep;
    }

    public void setSlicingStep(float slicingStep) {
        this.slicingStep = slicingStep;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getElevation() {
        return elevation;
    }

    @Override
    public String toString() {
        return "RenderArea{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", width=" + width +
                ", height=" + height +
                ", elevation=" + elevation +
                ", slicingStep=" + slicingStep +
                ", currentSlice=" + currentSlice +
                '}';
    }
}
