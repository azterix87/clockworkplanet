/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 8:50 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.lighting;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.RenderingSystem;
import self.azterix87.seraphim.systems.motionanimation.CMotionAnimator;

public class LightingModule implements RenderingModule {
    private FrameBuffer lightFBO;
    private TextureRegion lightMap;
    private ImmutableArray<Entity> entities;
    private Vector3 ambientLight;

    public LightingModule(Vector3 ambientLight) {
        this.ambientLight = ambientLight;
        lightFBO = FrameBuffer.createFrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        lightMap = new TextureRegion(lightFBO.getColorBufferTexture());
        lightMap.flip(false, true);
    }

    @Override
    public void addedToRenderingSystem(RenderingSystem renderingSystem) {
        Engine engine = renderingSystem.getEntityEngine();
        entities = engine.getEntitiesFor(Family.all(CLightmap.class, CTransform.class).get());
    }

    @Override
    public void preRender(GameContext gameContext, float dt) {
        //Render lightmap
        lightFBO.begin();
        Gdx.gl.glClearColor(ambientLight.x, ambientLight.y, ambientLight.z, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        //gameContext.spriteBatch().setProjectionMatrix(gameContext.getCurrentCamera().combined);
        gameContext.spriteBatch.begin();
        gameContext.spriteBatch.enableBlending();
        gameContext.spriteBatch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_COLOR);

        for (Entity e : entities) {
            CLightmap lmap = CLightmap.mapper.get(e);
            CTransform transform = CTransform.mapper.get(e);

            gameContext.spriteBatch.setTransformMatrix(transform.getTransformMatrix());
            Texture texture = gameContext.assets.getTexture(lmap.getLightmap());

            float scale = lmap.getScale();

            float offX = lmap.getOffset().x - scale * texture.getWidth() / 2;
            float offY = lmap.getOffset().y - scale * texture.getHeight() / 2;


            if (CMotionAnimator.mapper.has(e)) {
                Vector2 displace = CMotionAnimator.mapper.get(e).currentDisplace;
                offX += displace.x;
                offY += displace.y;
            }


            gameContext.spriteBatch.draw(texture, offX, offY, texture.getWidth() * scale, texture.getHeight() * scale);
        }
        gameContext.spriteBatch.disableBlending();
        gameContext.spriteBatch.end();

        lightFBO.end();

    }

    @Override
    public void postRender(GameContext gameContext, float dt) {
        //apply lightmap
        gameContext.spriteBatch.begin();

        //We don't need mapper and view matrices for lightmap
        gameContext.spriteBatch.setTransformMatrix(new Matrix4());
        gameContext.spriteBatch.setProjectionMatrix(gameContext.getCurrentCamera().projection);

        gameContext.spriteBatch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
        gameContext.spriteBatch.enableBlending();

        float scale = gameContext.getCurrentCamera().zoom;
        float width = gameContext.getCurrentCamera().viewportWidth;
        float height = gameContext.getCurrentCamera().viewportHeight;

        gameContext.spriteBatch.draw(lightMap, -width * scale / 2, -height * scale / 2,
                width * scale, height * scale);

        gameContext.spriteBatch.disableBlending();
        gameContext.spriteBatch.end();
    }
}
