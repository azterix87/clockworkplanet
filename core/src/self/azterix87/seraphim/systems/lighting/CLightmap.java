/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.lighting;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;


public class CLightmap implements Component {
    public static final ComponentMapper<CLightmap> mapper = ComponentMapper.getFor(CLightmap.class);
    private String lightmap;
    private Vector2 offset;
    private float scale = 1;

    public CLightmap() {
        offset = new Vector2();
    }

    public CLightmap(String lightmap, Vector2 offset) {
        this.lightmap = lightmap;
        this.offset = offset;
    }

    public CLightmap(String lightmap, float offsetX, float offsetY) {
        this(lightmap, new Vector2(offsetX, offsetY));
    }

    public CLightmap(String lightmap) {
        this(lightmap, 0, 0);
    }

    public String getLightmap() {
        return lightmap;
    }

    public void setLightmap(String lightmap) {
        this.lightmap = lightmap;
    }

    public Vector2 getOffset() {
        return offset;
    }

    public void setOffset(Vector2 offset) {
        this.offset = offset;
    }

    public void setOffset(float x, float y) {
        this.offset.set(x, y);
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
}
