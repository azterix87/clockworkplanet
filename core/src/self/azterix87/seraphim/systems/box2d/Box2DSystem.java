package self.azterix87.seraphim.systems.box2d;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import self.azterix87.seraphim.components.CTransform;

public class Box2DSystem extends EntitySystem {
    private World world;
    private ImmutableArray<Entity> entities;
    private EntityListener entityListener;

    public Box2DSystem(int priority) {
        super(priority);
        this.world = new World(new Vector2(0, 0), true);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        //Copy data from Entity to Box2d
        for (Entity entity : entities) {
            CTransform transform = CTransform.mapper.get(entity);
            Body body = CBox2DBody.mapper.get(entity).getBody();
            body.setTransform(transform.localPosition.x, transform.localPosition.y,
                    transform.rotation * MathUtils.degreesToRadians);
        }

        world.step(deltaTime, 6, 2);

        //Copy data from Box2d to Entity
        for (Entity entity : entities) {
            CTransform transform = CTransform.mapper.get(entity);
            Body body = CBox2DBody.mapper.get(entity).getBody();

            transform.localPosition.set(body.getPosition(), transform.localPosition.z);
            transform.rotation = MathUtils.radiansToDegrees * body.getAngle();
        }
    }

    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        this.entities = engine.getEntitiesFor(Family.all(CBox2DBody.class, CTransform.class).get());

        entityListener = new EntityListener() {
            @Override
            public void entityAdded(Entity entity) {
                CBox2DBody bodyComponent = CBox2DBody.mapper.get(entity);
                Body body = world.createBody(bodyComponent.getBodyDef());
                body.setUserData(entity);
                bodyComponent.getFixtureDefs().forEach(body::createFixture);
                bodyComponent.setBody(body);
            }

            @Override
            public void entityRemoved(Entity entity) {
                CBox2DBody bodyComponent = CBox2DBody.mapper.get(entity);
                world.destroyBody(bodyComponent.getBody());
                bodyComponent.setBody(null);
            }
        };

        engine.addEntityListener(Family.all(CBox2DBody.class, CTransform.class).get(), entityListener);
    }

    @Override
    public void removedFromEngine(Engine engine) {
        super.removedFromEngine(engine);
        engine.removeEntityListener(entityListener);
    }

    public World getWorld() {
        return world;
    }
}
