package self.azterix87.seraphim.systems.box2d;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CBox2DBody implements Component {
    public static final ComponentMapper<CBox2DBody> mapper = ComponentMapper.getFor(CBox2DBody.class);
    private final List<FixtureDef> fixtureDefs = new ArrayList<>();
    private BodyDef bodyDef;
    private Body body;


    public CBox2DBody(BodyDef bodyDef) {
        this.bodyDef = bodyDef;
    }

    public CBox2DBody(BodyDef bodyDef, FixtureDef... fixtures) {
        this.bodyDef = bodyDef;
        this.fixtureDefs.addAll(Arrays.asList(fixtures));
    }

    public Body getBody() {
        return body;
    }

    protected void setBody(Body body) {
        this.body = body;
    }

    public BodyDef getBodyDef() {
        return bodyDef;
    }

    public void addFixtureDef(FixtureDef fixtureDef) {
        fixtureDefs.add(fixtureDef);
    }

    public List<FixtureDef> getFixtureDefs() {
        return fixtureDefs;
    }
}
