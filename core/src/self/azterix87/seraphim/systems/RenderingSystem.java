/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:09 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Matrix4;
import self.azterix87.seraphim.GameContext;

import java.util.ArrayList;
import java.util.List;

public class RenderingSystem extends EntitySystem {
    public static final Matrix4 combineYZMatrix = new Matrix4(new float[]{
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 1, 0, 0,
            0, 0, 0, 1
    });

    private Engine entityEngine;
    private GameContext gameContext;
    private List<RenderingModule> modules = new ArrayList<>();
    private RenderArea renderArea;
    private float ppm = 1;

    public RenderingSystem(GameContext gameContext, RenderArea renderArea, float ppm, int priority) {
        super(priority);
        this.gameContext = gameContext;
        this.renderArea = renderArea;
        this.ppm = ppm;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        this.entityEngine = engine;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        //Return if we have no rendering modules
        if (modules.isEmpty()) {
            return;
        }

        //PRE-RENDER
        for (RenderingModule module : modules) {
            module.preRender(gameContext, deltaTime);
        }

        //RENDER
        while (renderArea.nextSlice()) {
            for (RenderingModule module : modules) {
                module.render(renderArea, gameContext, deltaTime);
            }
        }


        //POST-RENDER
        for (RenderingModule module : modules) {
            module.postRender(gameContext, deltaTime);
        }
    }

    public void requestSlicedRendering(float slicingStep) {
        renderArea.setSlicingStep(slicingStep);
    }

    public void addModule(RenderingModule renderingModule) {
        modules.add(renderingModule);
        renderingModule.addedToRenderingSystem(this);
    }

    public void removeModule(RenderingModule renderingModule) {
        modules.remove(renderingModule);
    }

    public Engine getEntityEngine() {
        return entityEngine;
    }

    public float getPPM() {
        return ppm;
    }
}
