/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 8:50 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.fonts;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.RenderingSystem;
import self.azterix87.seraphim.systems.motionanimation.CMotionAnimator;

public class FontRenderingModule implements RenderingModule {
    private ImmutableArray<Entity> entities;
    private RenderingSystem renderingSystem;

    @Override
    public void addedToRenderingSystem(RenderingSystem renderingSystem) {
        Engine engine = renderingSystem.getEntityEngine();
        entities = engine.getEntitiesFor(Family.all(CTransform.class, CText.class).get());
        this.renderingSystem = renderingSystem;
    }

    @Override
    public void postRender(GameContext gameContext, float dt) {
        float ppm = renderingSystem.getPPM();

        gameContext.spriteBatch.enableBlending();
        gameContext.spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        gameContext.spriteBatch.setProjectionMatrix(gameContext.getCurrentCamera().combined);
        gameContext.spriteBatch.begin();

        for (Entity e : entities) {
            CTransform transform = CTransform.mapper.get(e);
            CText textC = CText.mapper.get(e);

            Vector2 displace = textC.getOffset().cpy();

            if (CMotionAnimator.mapper.has(e)) {
                displace.add(CMotionAnimator.mapper.get(e).currentDisplace);
            }

            Matrix4 transformMatrix = transform.getTransformMatrix().cpy().scale(1 / ppm, 1 / ppm, 1);
            gameContext.spriteBatch.setTransformMatrix(transformMatrix);

            BitmapFont font = gameContext.assets.getFont(textC.getFont());
            font.draw(gameContext.spriteBatch, textC.getText(), displace.x, displace.y);
        }

        gameContext.spriteBatch.end();
        gameContext.spriteBatch.disableBlending();
    }
}
