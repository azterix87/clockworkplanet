/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems;

public interface AnimationListener {
    void onAnimationStarted(String animation);

    void onAnimationPaused(String animation);

    void onAnimationResumed(String animation);

    void onAnimationStopped(String animation);

    void onAnimationFinished(String animation);
}
