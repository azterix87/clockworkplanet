/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.spriterendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;

public class AnimationLoader extends SynchronousAssetLoader<Animation, AnimationLoader.AnimationLoaderParameters> {
    private XmlReader xmlReader;

    public AnimationLoader(FileHandleResolver resolver) {
        super(resolver);
        xmlReader = new XmlReader();
    }

    @Override
    public Animation load(AssetManager assetManager, String fileName, FileHandle file, AnimationLoaderParameters parameter) {
        XmlReader.Element root = null;
        try {
            root = xmlReader.parse(file);
        } catch (IOException e) {
            Gdx.app.error("@AnimationLoader", "Error loading " + file.path(), e);
        }

        Texture texture = assetManager.get(root.get("texture"), Texture.class);
        int tileH = root.getInt("tileHeight");
        int tileW = root.getInt("tileWidth");
        float frameDuration = root.getFloat("frameDuration");
        Animation.PlayMode mode = Animation.PlayMode.valueOf(root.get("mode"));

        String[] s_frames = root.get("frames").split(", ");
        int[] frames = new int[s_frames.length];

        for (int i = 0; i < frames.length; i++) {
            frames[i] = Integer.parseInt(s_frames[i]);
        }

        int tiles_h = texture.getWidth() / tileW;

        Array<TextureRegion> textureRegions = new Array<>();

        for (int frame : frames) {
            int x = frame % tiles_h;
            int y = frame / tiles_h;
            textureRegions.add(new TextureRegion(texture, x * tileW, y * tileH, tileW, tileH));
        }


        return new Animation(frameDuration, textureRegions, mode);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, AnimationLoaderParameters parameter) {
        Array<AssetDescriptor> deps = new Array<>();

        XmlReader.Element root;
        try {
            root = xmlReader.parse(file);
        } catch (IOException e) {
            return deps;
            //if there is an error no dependencies are required :D. Btw, we will report this error later.
        }

        deps.add(new AssetDescriptor(root.get("texture"), Texture.class));

        return deps;
    }

    public class AnimationLoaderParameters extends AssetLoaderParameters<Animation> {
    }
}
