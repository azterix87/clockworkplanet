/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:27 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.spriterendering;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.systems.AnimationPlayer;
import self.azterix87.seraphim.systems.RenderArea;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.RenderingSystem;
import self.azterix87.seraphim.systems.motionanimation.CMotionAnimator;

import java.util.ArrayList;
import java.util.List;

public class SpriteRenderingModule implements RenderingModule {
    private ImmutableArray<Entity> entities;
    private RenderingSystem renderingSystem;


    @Override
    public void addedToRenderingSystem(RenderingSystem renderingSystem) {
        Engine engine = renderingSystem.getEntityEngine();
        entities = engine.getEntitiesFor(Family.all(CSprite.class, CTransform.class).get());
        this.renderingSystem = renderingSystem;
    }

    @Override
    public void render(RenderArea renderArea, GameContext gameContext, float dt) {
        //Find entities within renderArea
        List<Entity> inboundEntities = getInboundEntities(renderArea);

        //Stop rendering if no entities in current slice
        if (inboundEntities.isEmpty()) {
            return;
        }

        //render current entities
        gameContext.spriteBatch.begin();
        gameContext.spriteBatch.setProjectionMatrix(gameContext.getCurrentCamera().combined);
        gameContext.spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        gameContext.spriteBatch.enableBlending();

        for (Entity e : inboundEntities) {
            CSprite sprite = CSprite.mapper.get(e);

            if (!sprite.isVisible()) {
                continue;
            }

            //Transform
            CTransform transform = CTransform.mapper.get(e);
            Matrix4 transformMatrix = RenderingSystem.combineYZMatrix.cpy().mul(transform.getTransformMatrix());
            gameContext.spriteBatch.setTransformMatrix(transformMatrix);

            //Animation
            updateSpriteAnimationPlayer(gameContext, sprite.getAnimationPlayer(), dt);

            //Color
            gameContext.spriteBatch.setColor(sprite.getColor());

            //Displace
            Vector2 displace = new Vector2(-sprite.getOriginX(), -sprite.getOriginY());

            if (CMotionAnimator.mapper.has(e)) {
                displace.add(CMotionAnimator.mapper.get(e).currentDisplace);
            }

            //Draw
            draw(sprite, displace, gameContext);
        }

        gameContext.spriteBatch.disableBlending();
        gameContext.spriteBatch.end();
    }

    private void draw(CSprite sprite, Vector2 displace, GameContext gameContext) {
        float PPM = renderingSystem.getPPM();

        if (sprite.getAnimationPlayer().hasAnimation()) {
            Animation animation = gameContext.assets.getAnimation(sprite.getAnimationPlayer().getCurrentAnimation());
            TextureRegion frame = (TextureRegion) animation.getKeyFrame(sprite.getAnimationPlayer().getCurrentDuration());

            float scx = sprite.isFlipX() ? -1 : 1;
            float scy = sprite.isFlipY() ? -1 : 1;

            gameContext.spriteBatch.draw(frame, displace.x / PPM, displace.y / PPM,
                    sprite.getOriginX() / PPM, sprite.getOriginY() / PPM,
                    frame.getRegionWidth() / PPM, frame.getRegionHeight() / PPM, scx, scy, sprite.getRotation());
        } else {
            Texture texture = gameContext.assets.getTexture(sprite.getSprite());

            gameContext.spriteBatch.draw(texture, displace.x / PPM, displace.y / PPM, sprite.getOriginX() / PPM,
                    sprite.getOriginY() / PPM, texture.getWidth() / PPM, texture.getHeight() / PPM,
                    1, 1, sprite.getRotation(), 0, 0, texture.getWidth(), texture.getHeight(),
                    sprite.isFlipX(), sprite.isFlipY());
        }
        gameContext.spriteBatch.setColor(Color.WHITE);
    }

    private List<Entity> getInboundEntities(RenderArea renderArea) {
        List<Entity> inboundEntities = new ArrayList<>();
        for (Entity e : entities) {
            Vector3 pos = CTransform.mapper.get(e).localPosition;

            if (renderArea.isInSliceBounds(pos.x, pos.y, pos.z)) {
                inboundEntities.add(e);
            }
        }

        //Sort entities
        inboundEntities.sort((o1, o2) -> {
            int sortingIndex1 = CSprite.mapper.get(o1).getSortingIndex();
            int sortingIndex2 = CSprite.mapper.get(o2).getSortingIndex();

            if (sortingIndex1 != sortingIndex2) {
                return sortingIndex1 - sortingIndex2;
            }

            Vector3 pos1 = CTransform.mapper.get(o1).getGlobalPosition();
            Vector3 pos2 = CTransform.mapper.get(o2).getGlobalPosition();

            if (pos1.y == pos2.y) {
                return 0;
            } else if (pos2.y > pos1.y) {
                return 1;
            } else {
                return -1;
            }
        });

        return inboundEntities;
    }

    private void updateSpriteAnimationPlayer(GameContext gameContext, AnimationPlayer animationPlayer, float dt) {
        animationPlayer.update(dt);
        if (animationPlayer.hasAnimation()) {
            Animation animation = gameContext.assets.getAnimation(animationPlayer.getCurrentAnimation());
            if ((animation.getPlayMode() == Animation.PlayMode.NORMAL || animation.getPlayMode() == Animation.PlayMode.REVERSED)
                    && animation.isAnimationFinished(animationPlayer.getCurrentDuration())) {
                animationPlayer.finishAnimation();
            }
        }
    }
}
