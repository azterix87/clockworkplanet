/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.spriterendering;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.graphics.Color;
import self.azterix87.seraphim.systems.AnimationPlayer;

public class CSprite implements Component {
    public static final ComponentMapper<CSprite> mapper = ComponentMapper.getFor(CSprite.class);
    private String sprite;
    private AnimationPlayer spriteAnimationPlayer;
    private Color color = Color.WHITE;
    private float originX;
    private float originY;
    private float rotation;
    private boolean flipX;
    private boolean flipY;
    private boolean visible = true;
    private int sortingIndex;
    //private boolean flipped;


    public CSprite() {
        spriteAnimationPlayer = new AnimationPlayer();
    }

    public String getSprite() {
        return sprite;
    }

    public void setSprite(String sprite) {
        this.sprite = sprite;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public float getOriginX() {
        return originX;
    }

    public void setOriginX(float originX) {
        this.originX = originX;
    }

    public float getOriginY() {
        return originY;
    }

    public void setOriginY(float originY) {
        this.originY = originY;
    }

    public AnimationPlayer getAnimationPlayer() {
        return spriteAnimationPlayer;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public boolean isFlipX() {
        return flipX;
    }

    public void setFlipX(boolean flipX) {
        this.flipX = flipX;
    }

    public boolean isFlipY() {
        return flipY;
    }

    public void setFlipY(boolean flipY) {
        this.flipY = flipY;
    }

    public int getSortingIndex() {
        return sortingIndex;
    }

    public void setSortingIndex(int sortingIndex) {
        this.sortingIndex = sortingIndex;
    }
}
