/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.systems.AnimationListener;
import self.azterix87.seraphim.systems.AnimationPlayer;

public class CMotionAnimator implements Component {
    public static final ComponentMapper<CMotionAnimator> mapper = ComponentMapper.getFor(CMotionAnimator.class);
    public final Vector2 currentDisplace;
    private AnimationPlayer animationPlayer;

    public CMotionAnimator() {
        this.animationPlayer = new AnimationPlayer();
        this.currentDisplace = new Vector2();
    }

    public AnimationPlayer getAnimationPlayer() {
        return animationPlayer;
    }

    public void setAnimation(String animation) {
        animationPlayer.setAnimation(animation);
    }

    public void setAnimationAndPlay(String animation) {
        animationPlayer.setAnimationAndPlay(animation);
    }

    public void play() {
        animationPlayer.play();
    }

    public void pause() {
        animationPlayer.pause();
    }

    public void reset() {
        animationPlayer.reset();
    }

    public void stop() {
        animationPlayer.stop();
    }

    public boolean addListener(AnimationListener animationListener) {
        return animationPlayer.addListener(animationListener);
    }

    public boolean removeListener(AnimationListener animationListener) {
        return animationPlayer.removeListener(animationListener);
    }
}

