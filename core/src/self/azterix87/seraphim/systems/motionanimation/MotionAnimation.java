/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.gdx.math.Vector2;

public interface MotionAnimation {
    Vector2 getDisplace(float time);

    boolean isFinished(float time);
}
