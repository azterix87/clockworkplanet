/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.systems.AnimationPlayer;

public class MotionAnimationSystem extends IteratingSystem {
    private GameContext gameContext;

    public MotionAnimationSystem(GameContext gameContext, int priority) {
        super(Family.all(CMotionAnimator.class).get(), priority);
        this.gameContext = gameContext;
    }

    @Override
    protected void processEntity(Entity entity, float dt) {
        CMotionAnimator animator = CMotionAnimator.mapper.get(entity);
        updateMotionAnimationPlayer(gameContext, animator.getAnimationPlayer(), dt);

        AnimationPlayer player = animator.getAnimationPlayer();

        if (player.hasAnimation()) {
            MotionAnimation mainm = gameContext.assets.getMotionAnimation(player.getCurrentAnimation());
            animator.currentDisplace.set(mainm.getDisplace(player.getCurrentDuration()));
        } else {
            animator.currentDisplace.setZero();
        }
    }

    private void updateMotionAnimationPlayer(GameContext gameContext, AnimationPlayer animationPlayer, float dt) {
        animationPlayer.update(dt);
        if (animationPlayer.hasAnimation()) {
            MotionAnimation animation = gameContext.assets.getMotionAnimation(animationPlayer.getCurrentAnimation());
            if (animation.isFinished(animationPlayer.getCurrentDuration())) {
                animationPlayer.finishAnimation();
            }
        }
    }

}
