/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;

public class MotionAnimationLoader extends SynchronousAssetLoader<MotionAnimation, MotionAnimationLoader.parameters> {
    private XmlReader xmlReader;

    public MotionAnimationLoader(FileHandleResolver resolver) {
        super(resolver);
        xmlReader = new XmlReader();
    }

    @Override
    public MotionAnimation load(AssetManager assetManager, String fileName, FileHandle file, parameters parameter) {
        XmlReader.Element root = null;
        try {
            root = xmlReader.parse(file);
        } catch (IOException e) {
            Gdx.app.error("@AnimationLoader", "Error loading " + file.path(), e);
        }

        float frameDuration = root.getFloat("frameDuration");
        boolean looped = root.getBoolean("looped");

        XmlReader.Element frames = root.getChildByName("frames");
        Array<XmlReader.Element> frameElements = frames.getChildrenByName("frame");

        float[][] data = new float[frameElements.size][2];

        for (int i = 0; i < frameElements.size; i++) {
            data[i][0] = frameElements.get(i).getFloatAttribute("x");
            data[i][1] = frameElements.get(i).getFloatAttribute("y");
        }

        return new FramedMotionAnimation(data, frameDuration, looped);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, parameters parameter) {
        return new Array<>();
    }

    public class parameters extends AssetLoaderParameters<MotionAnimation> {
    }
}
