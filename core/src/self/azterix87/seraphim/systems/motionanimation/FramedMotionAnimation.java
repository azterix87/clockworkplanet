/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems.motionanimation;

import com.badlogic.gdx.math.Vector2;

public class FramedMotionAnimation implements MotionAnimation {
    private float[][] displaces;
    private int totalFrames;
    private float frameDuration;
    private boolean looped;

    public FramedMotionAnimation(float[][] displaces, float frameDuration, boolean looped) {
        this.displaces = displaces;
        totalFrames = displaces.length;
        this.looped = looped;
        this.frameDuration = frameDuration;
    }

    @Override
    public Vector2 getDisplace(float time) {
        float timeElapsed = time;

        if (looped) {
            timeElapsed = timeElapsed % getDuration();
        }

        int frame = (int) (timeElapsed / frameDuration);


        if (frame < totalFrames) {
            return new Vector2(displaces[frame][0], displaces[frame][1]);
        }

        return new Vector2();
    }

    @Override
    public boolean isFinished(float time) {
        return !looped && (time) > getDuration();
    }

    public float getDuration() {
        return frameDuration * totalFrames;
    }


}
