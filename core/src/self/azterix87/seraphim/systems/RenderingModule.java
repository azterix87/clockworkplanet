/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:09 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems;

import self.azterix87.seraphim.GameContext;

public interface RenderingModule {

    default void addedToRenderingSystem(RenderingSystem renderingSystem) {

    }

    default void preRender(GameContext gameContext, float dt) {

    }

    default void render(RenderArea renderArea, GameContext gameContext, float dt) {

    }

    default void postRender(GameContext gameContext, float dt) {

    }
}
