/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.systems;

import java.util.ArrayList;
import java.util.List;

public class AnimationPlayer {
    private List<AnimationListener> listeners = new ArrayList<>();

    private String currentAnimation;
    private float currentDuration;
    private boolean paused;

    public void setAnimation(String animation) {
        if (currentAnimation != null) {
            stop();
        }
        this.currentAnimation = animation;
    }

    public void setAnimationAndPlay(String animation) {
        setAnimation(animation);
        play();
    }

    public void update(float dt) {
        if (paused) {
            return;
        }

        currentDuration += dt;
    }

    public void play() {
        paused = false;
        if (currentDuration == 0) {
            onAnimationStarted();
        } else {
            onAnimationResumed();
        }
    }

    public void pause() {
        paused = true;
        onAnimationPaused();
    }

    public void reset() {
        currentDuration = 0;
    }

    public void stop() {
        currentAnimation = null;
        reset();
        pause();
        onAnimationStopped();
    }

    /**
     * Called from apropriate system, when animation is finished
     */
    public void finishAnimation() {
        onAnimationFinished();
        currentAnimation = null;
        reset();
        pause();
    }

    public float getCurrentDuration() {
        return currentDuration;
    }

    public String getCurrentAnimation() {
        return currentAnimation;
    }

    public boolean hasAnimation() {
        return currentAnimation != null;
    }

    public boolean isPaused() {
        return paused;
    }

    private void onAnimationStarted() {
        for (AnimationListener listener : listeners) {
            listener.onAnimationStarted(currentAnimation);
        }
    }

    private void onAnimationResumed() {
        for (AnimationListener listener : listeners) {
            listener.onAnimationResumed(currentAnimation);
        }
    }

    private void onAnimationPaused() {
        for (AnimationListener listener : listeners) {
            listener.onAnimationPaused(currentAnimation);
        }
    }

    private void onAnimationFinished() {
        for (AnimationListener listener : listeners) {
            listener.onAnimationFinished(currentAnimation);
        }
    }

    private void onAnimationStopped() {
        for (AnimationListener listener : listeners) {
            listener.onAnimationStopped(currentAnimation);
        }
    }

    public boolean addListener(AnimationListener animationListener) {
        return listeners.add(animationListener);
    }

    public boolean removeListener(AnimationListener animationListener) {
        return listeners.remove(animationListener);
    }
}
