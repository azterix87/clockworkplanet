package self.azterix87.seraphim.tilemap;

public interface TilemapListener {
    default void regionAdded(Tilemap tilemap, TileRegion region) {

    }

    default void regionRemoved(Tilemap tilemap, TileRegion region) {

    }
}
