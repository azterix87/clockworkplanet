/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/9/17 9:28 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

public class RegionGenerator {
    private TileRegion tileRegion;

    public RegionGenerator(int width, int height, int elevation) {
        tileRegion = new TileRegion();
        tileRegion.setEmpty(width, height, elevation);
    }

    public RegionGenerator fillFloor(Tile tile) {
        short tileId = (short) tile.getId();
        for (int i = 0; i < tileRegion.getWidth(); i++) {
            for (int j = 0; j < tileRegion.getHeight(); j++) {
                tileRegion.setTileAt(i, j, 0, tileId);
            }

        }
        return this;
    }

    public RegionGenerator fill(int startX, int startY, int width, int height, int elevation, Tile tile) {
        for (int x = startX; x < startX + width; x++) {
            for (int y = startY; y < startY + height; y++) {
                tileRegion.setTileAt(x, y, elevation, tile.getId());
            }
        }
        return this;
    }

    public RegionGenerator setRandomWalls(Tile tile) {
        short tileId = (short) tile.getId();
        for (int i = 0; i < tileRegion.getWidth(); i++) {
            for (int j = 0; j < tileRegion.getHeight(); j++) {
                if (Math.random() > 0.7) {
                    tileRegion.setTileAt(i, j, 1, tileId);
                }
            }

        }
        return this;
    }


    public TileRegion get() {
        return tileRegion;
    }

}
