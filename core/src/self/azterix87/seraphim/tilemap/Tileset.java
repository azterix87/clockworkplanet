/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/9/17 5:56 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

import com.badlogic.gdx.Gdx;

public class Tileset {
    private Tile[] tiles;
    public static final Tile TILE_AIR = new Tile("air", "null", "");;

    public Tileset(int maxTiles) {
        tiles = new Tile[maxTiles];

        //add air tile with id 0
        tiles[0] = TILE_AIR;
    }

    public void addTile(int id, Tile tile) {
        //Check if tileset has space
        if (id >= tiles.length) {
            Gdx.app.error("@TileRegistry", "Can not add tile " + tile.getName() + " tile id out of bounds");
        }

        //If id is occupied with another tile, replace it and log message. else just add a tile
        if (tiles[id] != null) {
            Gdx.app.log("@TileRegistry", "Overriding tile [" + tiles[id].getName() + "] at id " +
                    id + " with tile [" + tile.getName() + "]");
        } else {
            Gdx.app.log("@TileRegistry", "Registered tile [" + tile.getName() + "] at id " + id);
        }

        tile.setId(id);
        tiles[id] = tile;
    }

    public Tile getTile(int id) {
        if (tiles[id] != null) {
            return tiles[id];
        } else {
            //If tile is not present, return 0 (air) tile
            Gdx.app.error("@TileRegistry", "No tile found with id " + id);
            tiles[id] = tiles[0];
            return tiles[0];
        }
    }

    public int getTileId(String tileName) {
        for (int i = 0; i < tiles.length; i++) {
            if (tiles[i] != null && tiles[i].getName().equals(tileName)) {
                return i;
            }
        }

        Gdx.app.error("@TileRegistry", "No tile with name " + tileName + " was found");

        return 0;
    }

    public Tile getTile(String tileName) {
        return getTile(getTileId(tileName));
    }
}
