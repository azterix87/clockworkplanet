/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 10:23 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

import java.util.Arrays;

public class Tile {
    private int id;
    private String name;
    private String TextureAtlas;
    private String[] textures;

    public Tile(String name, String textureAtlas, String... textures) {
        this.name = name;
        TextureAtlas = textureAtlas;
        this.textures = textures;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextureAtlas() {
        return TextureAtlas;
    }

    public void setTextureAtlas(String textureAtlas) {
        TextureAtlas = textureAtlas;
    }

    public String[] getTextures() {
        return textures;
    }

    public String getTexture() {
        return textures[0];
    }

    public String getTexture(int subId) {
        return getTexture();
    }

    public boolean hasMultiTextures() {
        return getTextures().length > 0;
    }

    public int getId() {
        return id;
    }

    protected void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Tile{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", TextureAtlas='" + TextureAtlas + '\'' +
                ", textures=" + Arrays.toString(textures) +
                '}';
    }
}
