/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 1:20 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

public class TilemapProperties {
    public final float unitWidth;
    public final float unitHeight;
    public final float unitElevation;

    public TilemapProperties(float unitWidth, float unitHeight, float unitElevation) {
        this.unitWidth = unitWidth;
        this.unitHeight = unitHeight;
        this.unitElevation = unitElevation;
    }
}
