/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:43 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

import java.util.ArrayList;
import java.util.List;

public class Tilemap {
    private final List<TileRegion> currentRegions = new ArrayList<>();
    private final List<TilemapListener> listeners = new ArrayList<>();
    public final TilemapProperties tilemapProperties;
    public final Tileset tileset;

    public Tilemap(TilemapProperties tilemapProperties, Tileset tileset) {
        this.tileset = tileset;
        this.tilemapProperties = tilemapProperties;
    }

    public float getFloorHeightAt(float x, float y) {
        int tileX = toTileX(x);
        int tileY = toTileY(y);
        TileRegion region = regionAt(tileX, tileY, 0);

        if (region != null) {
            for (int i = region.getHeight() - 1; i >= 0; i--) {
                if (getTile(tileX, tileY, i) != tileset.TILE_AIR) {
                    return toWorldZ(i + 1);
                }
            }
        }

        return 0;
    }

    public Tile getTileAt(float x, float y, float z) {
        int tileX = (int) (x / tilemapProperties.unitWidth);
        int tileY = (int) (y / tilemapProperties.unitHeight);
        int tileZ = (int) (z / tilemapProperties.unitElevation);

        return getTile(tileX, tileY, tileZ);
    }

    public Tile getTile(int tileX, int tileY, int tileZ) {
        TileRegion region = regionAt(tileX, tileY, tileZ);

        if (region != null) {
            return tileset.getTile(region.getTileAt(tileX - region.getRegionX(), tileY - region.getRegionY(), tileZ));
        } else {
            //If point does not belong to anu regions return air
            return Tileset.TILE_AIR;
        }
    }

    public void addRegion(TileRegion tileRegion) {
        if (!currentRegions.contains(tileRegion)) {
            currentRegions.add(tileRegion);

            listeners.forEach((e) -> e.regionAdded(this, tileRegion));
        }
    }

    protected TileRegion regionAt(int tileX, int tileY, int tileZ) {
        for (TileRegion region : currentRegions) {
            if (region.isInBoundsGlobal(tileX, tileY, tileZ)) {
                return region;
            }
        }
        return null;
    }

    public void addListener(TilemapListener tilemapListener) {
        listeners.add(tilemapListener);
    }

    public void removeListener(TilemapListener tilemapListener) {
        listeners.remove(tilemapListener);
    }

    protected int toTileX(float x) {
        return (int) (x / tilemapProperties.unitWidth);
    }

    protected int toTileY(float y) {
        return (int) (y / tilemapProperties.unitHeight);
    }

    protected int toTileZ(float z) {
        return (int) (z / tilemapProperties.unitElevation);
    }

    public float toWorldX(int tileX) {
        return tileX * tilemapProperties.unitWidth;
    }

    public float toWorldY(int tileY) {
        return tileY * tilemapProperties.unitHeight;
    }

    public float toWorldZ(int tileZ) {
        return tileZ * tilemapProperties.unitElevation;
    }
}
