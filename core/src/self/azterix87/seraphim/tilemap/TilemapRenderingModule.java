/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:39 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.systems.RenderArea;
import self.azterix87.seraphim.systems.RenderingModule;
import self.azterix87.seraphim.systems.RenderingSystem;

public class TilemapRenderingModule implements RenderingModule {
    private Tilemap tilemap;

    public TilemapRenderingModule(Tilemap tilemap) {
        this.tilemap = tilemap;
    }

    @Override
    public void addedToRenderingSystem(RenderingSystem renderingSystem) {
        renderingSystem.requestSlicedRendering(tilemap.tilemapProperties.unitHeight);
    }

    @Override
    public void render(RenderArea renderArea, GameContext gameContext, float dt) {
        SpriteBatch spriteBatch = gameContext.spriteBatch;
        spriteBatch.setProjectionMatrix(gameContext.getCurrentCamera().combined);
        spriteBatch.setTransformMatrix(new Matrix4());
        spriteBatch.enableBlending();
        spriteBatch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        spriteBatch.begin();

        //Coordinate transformation


        int minTileX = (int) (renderArea.getX() / tilemap.tilemapProperties.unitWidth);
        int maxTileX = (int) ((renderArea.getX() + renderArea.getWidth()) / tilemap.tilemapProperties.unitWidth);

        int minTileZ = (int) (renderArea.getZ() / tilemap.tilemapProperties.unitElevation);
        int maxTileZ = (int) ((renderArea.getZ() + renderArea.getElevation()) / tilemap.tilemapProperties.unitElevation);

        int y = (int) ((renderArea.currentSliceY()) / tilemap.tilemapProperties.unitHeight) - 1;

        for (int x = minTileX; x <= maxTileX; x++) {
            for (int z = minTileZ; z < maxTileZ; z++) {
                Tile tile = tilemap.getTile(x, y, z);

                //Don't render air tiles lol
                if (tile == Tileset.TILE_AIR) {
                    continue;
                }

                //Find tiles texture atlas and get tile's texture from it
                TextureAtlas atlas = gameContext.assets.getTextureAtlas(tile.getTextureAtlas());
                TextureRegion region = atlas.findRegion(tile.getTexture());

                float unitWidth = tilemap.tilemapProperties.unitWidth;
                float unitHeight = tilemap.tilemapProperties.unitHeight;
                float unitElevation = tilemap.tilemapProperties.unitElevation;

                if(region == null) {
                    region = new TextureRegion(gameContext.assets.getDefaultTexture());
                }

                float aspectRatio = region.getRegionHeight() / (float) region.getRegionWidth();
                spriteBatch.draw(region, x * unitWidth, y * unitHeight + z * unitElevation, unitWidth, unitWidth * aspectRatio);
            }
        }

        //end rendering
        spriteBatch.end();
        spriteBatch.disableBlending();
    }
}
