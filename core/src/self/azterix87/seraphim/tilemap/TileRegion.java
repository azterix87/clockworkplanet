/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/9/17 7:35 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.seraphim.tilemap;

public class TileRegion {
    private int regionX;
    private int regionY;
    private int width;
    private int height;
    private int elevation;

    private int[] tiles;

    protected void setTileAt(int x, int y, int z, int tile) {
        tiles[coordinatesToIndex(x, y, z)] = tile;
    }

    public int getTileAt(int x, int y, int z) {
        return tiles[coordinatesToIndex(x, y, z)];
    }

    protected void setData(int[] data, int width, int height, int elevation) {
        if (data.length != width * height * elevation) {
            throw new IllegalArgumentException("Length of tile data does not correspond to bounds");
        }

        tiles = data;
        setBounds(width, height, elevation);
    }

    protected void setEmpty(int width, int height, int elevation) {
        tiles = new int[width * height * elevation];
        setBounds(width, height, elevation);
    }

    private int coordinatesToIndex(int x, int y, int z) {
        //Check for bounds
        if (!isInBounds(x, y, z)) {
            throw new IllegalArgumentException("Coordinates out of region bounds");
        }

        //calculate index;
        int index = 0;
        index += width * height * z;
        index += width * y;
        index += x;

        return index;
    }

    public boolean isInBoundsGlobal(int x, int y, int z) {
        x = x - this.regionX;
        y = y - this.regionY;

        return isInBounds(x, y, z);
    }

    private boolean isInBounds(int x, int y, int z) {
        return x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < elevation;
    }

    protected void setPosition(int x, int y) {
        this.regionX = x;
        this.regionY = y;
    }

    private void setBounds(int width, int height, int elevation) {
        this.width = width;
        this.height = height;
        this.elevation = elevation;
    }

    public int getRegionX() {
        return regionX;
    }

    public int getRegionY() {
        return regionY;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getElevation() {
        return elevation;
    }
}
