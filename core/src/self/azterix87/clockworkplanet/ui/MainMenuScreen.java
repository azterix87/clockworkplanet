/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import self.azterix87.seraphim.gui.BasicLayout;
import self.azterix87.seraphim.gui.GUIScreen;

public class MainMenuScreen extends GUIScreen {
    Button bPlay;
    Button bExit;

    public MainMenuScreen(Skin skin) {
        super();

        Table root = new Table();
        root.setFillParent(true);
        root.defaults().space(5);

        bPlay = new TextButton("Play", skin);
        bPlay.setWidth(100);
        bExit = new TextButton("Exit", skin);
        bExit.setWidth(100);

        bExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });


        root.add(bPlay);
        root.row();
        root.add(bExit);

        gui.setLayout(new BasicLayout(root));
    }

    public void setOnPlay(Runnable r) {
        bPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                r.run();
            }
        });
    }
}
