/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import self.azterix87.seraphim.gui.BasicLayout;
import self.azterix87.seraphim.gui.GUIScreen;

public class LoadingScreen extends GUIScreen {
    private Label loadingLabel;
    private ProgressBar progressBar;
    private Runnable onUpdate;

    public LoadingScreen(Skin skin) {
        Table root = new Table();

        root.setFillParent(true);
        root.defaults().space(5);

        loadingLabel = new Label("Loading...", skin);
        progressBar = new ProgressBar(0F, 1F, 0.01F, false, skin);

        root.add(loadingLabel);
        root.row();
        root.add(progressBar);

        gui.setLayout(new BasicLayout(root));
    }


    public void setProgress(float progress) {
        progressBar.setValue(progress);
    }

    public void setLoadingFile(String file) {
        loadingLabel.setText("Loading: " + file);
    }

    public void setOnUpdate(Runnable onUpdate) {
        this.onUpdate = onUpdate;
    }

    public void reset() {
        progressBar.setValue(0);
        loadingLabel.setText("Loading...");
        onUpdate = null;
    }

    @Override
    public void hide() {
        super.hide();
        reset();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if (onUpdate != null) {
            onUpdate.run();
        }
    }
}
