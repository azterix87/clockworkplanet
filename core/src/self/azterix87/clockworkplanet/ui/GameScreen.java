/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 12:40 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet.ui;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import self.azterix87.seraphim.GameContext;

public class GameScreen implements Screen {
    private GameContext gameContext;

    public GameScreen(GameContext gameContext) {
        this.gameContext = gameContext;
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(gameContext.getInputProcessor());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(Gdx.gl.GL_COLOR_BUFFER_BIT);

        gameContext.update(delta);
    }

    @Override
    public void resize(int width, int height) {
        OrthographicCamera camera = gameContext.getCurrentCamera();
        camera.viewportWidth = camera.viewportHeight * (width / (float) height);
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {

    }
}
