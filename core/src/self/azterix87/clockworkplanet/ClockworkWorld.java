/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:41 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.clockworkplanet.factories.EnemyFactory;
import self.azterix87.clockworkplanet.factories.PlayerFactory;
import self.azterix87.seraphim.Box2DWorld;
import self.azterix87.seraphim.CameraRenderBoundsController;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.scripts.ScriptSystem;
import self.azterix87.seraphim.systems.RenderArea;
import self.azterix87.seraphim.systems.RenderingSystem;
import self.azterix87.seraphim.systems.fonts.FontRenderingModule;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimationSystem;
import self.azterix87.seraphim.systems.spriterendering.SpriteRenderingModule;
import self.azterix87.seraphim.tiled.TiledHelper;
import self.azterix87.seraphim.tiled.TiledRenderingModule;

public class ClockworkWorld extends Box2DWorld {
    public static final float ppm = 32;
    private ScriptSystem scriptSystem;
    private MotionAnimationSystem motionAnimationSystem;
    private RenderingSystem renderingSystem;
    private TiledRenderingModule tiledRenderingModule;
    private SpriteRenderingModule spriteRenderingModule;
    private FontRenderingModule fontRenderingModule;

    private CameraRenderBoundsController renderAreaController;
    //private Tilemap tilemap;
    private TiledMap map;


    public ClockworkWorld(GameContext gameContext) {
        super(gameContext);
    }

    public void startNewGame() {
        Entity player = spawnEntity("player", new Vector3(5, 5, 0), null);
        PlayerController playerController = new PlayerController(player, gameContext);
        gameContext.addController(playerController);

        //Attach camera to player
        FollowingCameraController camController = new FollowingCameraController(gameContext.getCurrentCamera());
        camController.setTarget(player);
        gameContext.addController(camController);

        //load tilemap
        map = gameContext.assets.getAsset("assets/maps/testmap.tmx", TiledMap.class);
        tiledRenderingModule.setMap(map);
        TiledHelper.generateCollisionBoxes(map, this);
    }

    @Override
    public void init() {
        super.init();

        //Create camera
        float aspectRatio = Gdx.graphics.getWidth() / Gdx.graphics.getHeight();
        OrthographicCamera camera = new OrthographicCamera(15 * aspectRatio, 15);
        gameContext.setCurrentCamera(camera);
        gameContext.addController(new DebugController());
    }

    @Override
    protected void initFactories() {
        Gdx.app.log("@Game", "Initializing factories");
        gameContext.factoryManager.registerFactory(new PlayerFactory());
        gameContext.factoryManager.registerFactory(new EnemyFactory());
    }

    @Override
    protected void initSystems() {
        super.initSystems();

        Gdx.app.log("@Game", "Initializing Systems..");
        Gdx.app.log("@Game", "Initializing script systems");
        scriptSystem = new ScriptSystem(gameContext, 1);
        entityEngine.addSystem(scriptSystem);

        Gdx.app.log("@Game", "Initializing rendering and animation systems");
        //Motion animation
        motionAnimationSystem = new MotionAnimationSystem(gameContext, 2);
        entityEngine.addSystem(motionAnimationSystem);

        //RenderArea
        RenderArea renderArea = new RenderArea(0, 0, 300);
        renderAreaController = new CameraRenderBoundsController(renderArea);
        renderAreaController.setBorder(50);
        gameContext.addController(renderAreaController);

        //Rendering system
        renderingSystem = new RenderingSystem(gameContext, renderArea, ppm, 3);
        entityEngine.addSystem(renderingSystem);

        //Tile rendering module
        tiledRenderingModule = new TiledRenderingModule();
        renderingSystem.addModule(tiledRenderingModule);

        //Sprite rendering module
        spriteRenderingModule = new SpriteRenderingModule();
        renderingSystem.addModule(spriteRenderingModule);

        //Lighting
        //renderingSystem.addModule(new LightingModule(new Vector3(0.07F, 0.07F, 0.07F)));

        //Fonts
        fontRenderingModule = new FontRenderingModule();
        renderingSystem.addModule(fontRenderingModule);

        //Debug
        entityEngine.addSystem(new DebugRenderingSystem(gameContext));
    }

}
