package self.azterix87.clockworkplanet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameContext;

public class DebugController implements Controller {
    private GameContext gameContext;

    @Override
    public void initialize(GameContext gameContext) {
        this.gameContext = gameContext;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector3 cords = gameContext.getCurrentCamera().unproject(new Vector3(screenX, screenY, 0));
        if(button == 1 && Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
            for (int i = 0; i < 1; i++) {
                gameContext.getGameWorld().spawnEntity("goose", cords, null);
            }
            return true;
        }

        return false;
    }
}
