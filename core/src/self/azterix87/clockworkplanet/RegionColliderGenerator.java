package self.azterix87.clockworkplanet;

import com.badlogic.ashley.core.Entity;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.tilemap.TileRegion;
import self.azterix87.seraphim.tilemap.Tilemap;
import self.azterix87.seraphim.tilemap.TilemapListener;
import self.azterix87.seraphim.tilemap.Tileset;

public class RegionColliderGenerator implements TilemapListener {
    private GameContext gameContext;

    public RegionColliderGenerator(GameContext gameContext) {
        this.gameContext = gameContext;
    }

    @Override
    public void regionAdded(Tilemap tilemap, TileRegion region) {
        for(int x = 0; x < region.getWidth(); x++) {
            for(int y=0; y < region.getHeight(); y++) {
                if(region.getTileAt(x, y, 1) != Tileset.TILE_AIR.getId()) {
                    Entity colliderEntity = new Entity();
                    CTransform transform = new CTransform();
                    transform.localPosition.x = tilemap.toWorldX(x);
                    transform.localPosition.y = tilemap.toWorldY(y);
                    transform.localPosition.z = tilemap.toWorldZ(1);
                    colliderEntity.add(transform);
                    //Collider collider = ColliderFactory.getBoxCollider(tilemap.tilemapProperties.unitWidth, tilemap.tilemapProperties.unitHeight);
                    //colliderEntity.add(collider);
                    gameContext.getGameWorld().addEntity(colliderEntity);
                }
            }
        }
    }
}
