package self.azterix87.clockworkplanet.scripts;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.GameWorld;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.scripts.CScriptContainer;
import self.azterix87.seraphim.scripts.ComponentAccessException;
import self.azterix87.seraphim.scripts.Script;
import self.azterix87.seraphim.systems.box2d.CBox2DBody;
import self.azterix87.seraphim.systems.fonts.CText;

public class GooseScript extends Script {
    private CTransform transform;
    private CBox2DBody physicalBodyComponent;
    private CText text;
    private WalkingEntityScript walkingEntityScript;
    @Override
    public void link(CScriptContainer scriptContainer) throws ComponentAccessException {
        this.transform = scriptContainer.getComponent(CTransform.mapper);
        this.physicalBodyComponent = scriptContainer.getComponent(CBox2DBody.mapper);
        this.text = scriptContainer.getComponent(CText.mapper);
        this.walkingEntityScript = scriptContainer.getScript(WalkingEntityScript.class);
    }

    @Override
    public void update(GameWorld gameWorld, float dt) {
        Entity player = gameWorld.findEntityWithTag("player");
        if(player != null) {
            Vector3 playerPos = CTransform.mapper.get(player).localPosition;

            Vector2 diff = new Vector2(playerPos.x - transform.localPosition.x, playerPos.y - transform.localPosition.y);

            walkingEntityScript.setWalkingDirection(diff.nor());
            //physicalBodyComponent.getBody().applyForceToCenter(dir.setLength(walkForceMultiplier * moveSpeed), true);
        }
    }

    @Override
    public void destroy(GameWorld gameWorld) {

    }
}
