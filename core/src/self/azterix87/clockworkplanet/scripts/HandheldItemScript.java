package self.azterix87.clockworkplanet.scripts;

import com.badlogic.ashley.core.Entity;
import self.azterix87.clockworkplanet.items.Item;
import self.azterix87.seraphim.GameWorld;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.scripts.CScriptContainer;
import self.azterix87.seraphim.scripts.ComponentAccessException;
import self.azterix87.seraphim.scripts.Script;
import self.azterix87.seraphim.systems.spriterendering.CSprite;


public class HandheldItemScript extends Script {
    private float itemOffsetX;
    private float itemOffsetY;
    private Item currentItem;

    private Entity itemEntity;
    private CSprite itemEntitySprite;
    private CTransform itemEntityTransform;

    public HandheldItemScript(float itemOffsetX, float itemOffsetY) {
        this.itemOffsetX = itemOffsetX;
        this.itemOffsetY = itemOffsetY;

        //Create item entity
        itemEntity = new Entity();

        itemEntityTransform = new CTransform();
        itemEntityTransform.localPosition.set(itemOffsetX, itemOffsetY, 0);
        itemEntity.add(itemEntityTransform);

        itemEntitySprite = new CSprite();
        itemEntitySprite.setSortingIndex(1);
        itemEntity.add(itemEntitySprite);

        //TODO REMOVE
        itemEntitySprite.setOriginX(16);
    }

    @Override
    public void link(CScriptContainer scriptContainer) throws ComponentAccessException {
        itemEntityTransform.setParent(scriptContainer.getComponent(CTransform.mapper));
    }

    @Override
    public void firstUpdate(GameWorld gameWorld, float dt) {
        gameWorld.addEntity(itemEntity);
    }

    @Override
    public void update(GameWorld gameWorld, float dt) {
        super.update(gameWorld, dt);
    }

    public void setFlipped(boolean flipped) {
        itemEntitySprite.setFlipX(flipped);
        itemEntityTransform.localPosition.x = flipped ? -itemOffsetX : itemOffsetX;
    }

    public Item getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(Item newItem) {
        this.currentItem = newItem;
        if (newItem != null) {
            itemEntitySprite.setSprite(newItem.getTexture());
        }
    }

    @Override
    public void destroy(GameWorld gameWorld) {
        gameWorld.removeEntity(itemEntity);
    }
}
