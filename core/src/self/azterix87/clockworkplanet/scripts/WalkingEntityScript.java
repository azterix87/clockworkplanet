package self.azterix87.clockworkplanet.scripts;

import com.badlogic.gdx.math.Vector2;
import self.azterix87.seraphim.GameWorld;
import self.azterix87.seraphim.scripts.CScriptContainer;
import self.azterix87.seraphim.scripts.ComponentAccessException;
import self.azterix87.seraphim.scripts.Script;
import self.azterix87.seraphim.systems.box2d.CBox2DBody;
import self.azterix87.seraphim.systems.spriterendering.CSprite;

public class WalkingEntityScript extends Script {
    public static final int WALKING_RIGHT = 0;
    public static final int WALKING_LEFT = 1;

    public static final float maxAcceleration = 15F;
    private final Vector2 walkingDirection;
    private float walkingSpeed;
    private CBox2DBody bodyComponent;
    private CSprite sprite;
    private int walkingOrientation;

    public WalkingEntityScript(float walkingSpeed) {
        this.walkingSpeed = walkingSpeed;
        this.walkingDirection = new Vector2();
    }

    @Override
    public void link(CScriptContainer scriptContainer) throws ComponentAccessException {
        bodyComponent = scriptContainer.getComponent(CBox2DBody.mapper);
        sprite = scriptContainer.getComponent(CSprite.mapper);
    }

    @Override
    public void update(GameWorld gameWorld, float dt) {
        Vector2 currentVelocity = bodyComponent.getBody().getLinearVelocity();
        Vector2 diff = walkingDirection.cpy().scl(walkingSpeed).sub(currentVelocity);

        if (diff.len() > maxAcceleration * dt) {
            diff.setLength(maxAcceleration * dt);
        }


        bodyComponent.getBody().setLinearVelocity(diff.add(currentVelocity));

        if (diff.len2() > 0) {
            bodyComponent.getBody().setAwake(true);
        }
    }

    @Override
    public void destroy(GameWorld gameWorld) {

    }

    public float getWalkingSpeed() {
        return walkingSpeed;
    }

    public Vector2 getWalkingDirection() {
        return walkingDirection;
    }

    public void setWalkingDirection(Vector2 walkingDirection) {
        this.walkingDirection.set(walkingDirection);

        if (walkingDirection.x < 0) {
            sprite.setFlipX(true);
            walkingOrientation = WALKING_LEFT;
        } else if (walkingDirection.x > 0) {
            sprite.setFlipX(false);
            walkingOrientation = WALKING_RIGHT;
        }
    }

    public int getWalkingOrientation() {
        return walkingOrientation;
    }
}
