/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/11/17 1:00 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.CTransform;

public class FollowingCameraController implements Controller {
        private OrthographicCamera camera;
        private Entity target;

    public FollowingCameraController(OrthographicCamera camera) {
            this.camera = camera;
        }

        @Override
        public void update(GameContext gameContext, float delta) {
            if (target == null) {
                return;
            }

            Vector3 targetPos = CTransform.mapper.get(target).localPosition;

            camera.position.set(targetPos.x, targetPos.y, camera.position.z);
            camera.update();
        }

        public Entity getTarget() {
            return target;
        }

        public void setTarget(Entity target) {
            this.target = target;
        }
}
