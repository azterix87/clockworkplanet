/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/10/17 3:29 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import self.azterix87.clockworkplanet.scripts.HandheldItemScript;
import self.azterix87.clockworkplanet.scripts.WalkingEntityScript;
import self.azterix87.seraphim.ParametricEntityFactory;
import self.azterix87.seraphim.components.CTags;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.scripts.CScriptContainer;
import self.azterix87.seraphim.systems.box2d.CBox2DBody;
import self.azterix87.seraphim.systems.spriterendering.CSprite;

import java.util.Map;

public class PlayerFactory implements ParametricEntityFactory {
    private CircleShape colliderShape;

    public PlayerFactory() {
        this.colliderShape = new CircleShape();
        colliderShape.setRadius(0.3F);
    }

    @Override
    public Entity createEntity(String type, Vector3 position, Map<String, String> parameters) {
        Entity player = new Entity();

        CTransform transform = new CTransform();
        transform.localPosition.set(position);
        player.add(transform);

        CSprite spriteRenderable = new CSprite();
        spriteRenderable.setSprite("assets/textures/testPlayer.png");
        spriteRenderable.setOriginX(16);
        spriteRenderable.setOriginY(3);
        player.add(spriteRenderable);


        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.fixedRotation = true;


        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = colliderShape;
        fixtureDef.density = 250;

        CBox2DBody bodyComponent = new CBox2DBody(bodyDef, fixtureDef);
        player.add(bodyComponent);

        CTags tags = new CTags();
        tags.addTag("player");
        player.add(tags);

        CScriptContainer scriptContainer = new CScriptContainer(player);
        scriptContainer.addScript(new WalkingEntityScript(4));
        scriptContainer.addScript(new HandheldItemScript(0.75F, 0.2F));
        player.add(scriptContainer);

        return player;
    }

    @Override
    public String[] getSupportedEntityTypes() {
        return new String[]{"player"};
    }
}
