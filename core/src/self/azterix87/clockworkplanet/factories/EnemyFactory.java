package self.azterix87.clockworkplanet.factories;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import self.azterix87.clockworkplanet.scripts.GooseScript;
import self.azterix87.clockworkplanet.scripts.WalkingEntityScript;
import self.azterix87.seraphim.ParametricEntityFactory;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.scripts.CScriptContainer;
import self.azterix87.seraphim.systems.box2d.CBox2DBody;
import self.azterix87.seraphim.systems.fonts.CText;
import self.azterix87.seraphim.systems.spriterendering.CSprite;

import java.util.Map;

public class EnemyFactory implements ParametricEntityFactory {
    private CircleShape circle;

    public EnemyFactory() {
        this.circle = new CircleShape();
        this.circle.setRadius(0.3F);
        this.circle.setPosition(new Vector2(0, 0));
    }

    @Override
    public Entity createEntity(String type, Vector3 position, Map<String, String> parameters) {
        switch (type) {
            case "goose":
                return createGoose(position);
            default:
                return null;
        }
    }

    private Entity createGoose(Vector3 pos) {
        Entity goose = new Entity();

        CTransform transform = new CTransform();
        transform.localPosition.set(pos);
        goose.add(transform);

        CSprite renderable = new CSprite();
        renderable.setSprite("assets/textures/gooseEnemy.png");
        renderable.setOriginX(16);
        renderable.setOriginY(12);
        goose.add(renderable);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.fixedRotation = true;

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circle;
        fixtureDef.density = 50F;

        CBox2DBody bodyComponent = new CBox2DBody(bodyDef, fixtureDef);
        goose.add(bodyComponent);

        CText text = new CText("regular");
        text.setOffset(new Vector2(0, 16));
        goose.add(text);

        CScriptContainer scriptContainer = new CScriptContainer(goose);
        scriptContainer.addScript(new WalkingEntityScript(2));
        scriptContainer.addScript(new GooseScript());
        goose.add(scriptContainer);

        return goose;
    }

    @Override
    public String[] getSupportedEntityTypes() {
        return new String[] {"goose"};
    }
}
