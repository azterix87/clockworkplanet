/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/11/17 1:57 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import self.azterix87.seraphim.Box2DWorld;
import self.azterix87.seraphim.GameContext;

public class DebugRenderingSystem extends EntitySystem {
    private GameContext gameContext;
    private Box2DDebugRenderer renderer;

    public DebugRenderingSystem(GameContext gameContext) {
        super(Integer.MAX_VALUE);
        this.gameContext = gameContext;
        this.renderer = new Box2DDebugRenderer();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        World world = ((Box2DWorld) gameContext.getGameWorld()).getBox2dWorld();
        renderer.render(world, gameContext.getCurrentCamera().combined);
    }
}
