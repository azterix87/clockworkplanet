/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/11/17 12:37 AM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import self.azterix87.clockworkplanet.items.Weapon;
import self.azterix87.clockworkplanet.items.WeaponSword;
import self.azterix87.clockworkplanet.scripts.HandheldItemScript;
import self.azterix87.clockworkplanet.scripts.WalkingEntityScript;
import self.azterix87.seraphim.Controller;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.scripts.CScriptContainer;
import self.azterix87.seraphim.scripts.ComponentAccessException;


public class PlayerController implements Controller {

    private WalkingEntityScript walkingEntityScript;
    private HandheldItemScript handheldItemScript;
    private GameContext gameContext;
    private Entity player;
    private Vector2  moveDir = new Vector2(0, 0);
    private Weapon currentWeapon;

    public PlayerController(Entity player, GameContext gameContext) {
        this.player = player;
        this.gameContext = gameContext;
        this.currentWeapon = new WeaponSword("assets/textures/sword.png");

        try {
            walkingEntityScript = CScriptContainer.mapper.get(player).getScript(WalkingEntityScript.class);
            handheldItemScript = CScriptContainer.mapper.get(player).getScript(HandheldItemScript.class);
        } catch (ComponentAccessException e) {
            Gdx.app.error("@PLayerController", "It seems the player has lost his limbs");
        }

        this.handheldItemScript.setCurrentItem(currentWeapon);
    }

    @Override
    public void update(GameContext gameContext, float delta) {
        walkingEntityScript.setWalkingDirection(moveDir.cpy().nor());

        if (currentWeapon != null) {
            currentWeapon.update(delta);
        }

        handheldItemScript.setFlipped(walkingEntityScript.getWalkingOrientation() == WalkingEntityScript.WALKING_LEFT);
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.W) {
            moveDir.y += 1;
        } else if (keycode == Input.Keys.S) {
            moveDir.y -= 1;
        } else if (keycode == Input.Keys.A) {
            moveDir.x -= 1;
        } else if (keycode == Input.Keys.D) {
            moveDir.x += 1;
        } else {
            return false;
        }

        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.W) {
            moveDir.y -= 1;
        } else if (keycode == Input.Keys.S) {
            moveDir.y += 1;
        } else if (keycode == Input.Keys.A) {
            moveDir.x += 1;
        } else if (keycode == Input.Keys.D) {
            moveDir.x -= 1;
        } else {
            return false;
        }
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == 0 && currentWeapon != null) {
            currentWeapon.attack(getAttackDirection(screenX, screenY), this);
            return true;
        }

        return false;
    }

    private Vector2 getAttackDirection(int screenX, int screenY) {
        Vector3 screenPoint = gameContext.getCurrentCamera().unproject(new Vector3(screenX, screenY, 0));
        Vector3 playerPos = CTransform.mapper.get(player).localPosition;
        return new Vector2(screenPoint.x - playerPos.x, screenPoint.y - playerPos.y).nor();
    }

    public GameContext getGameContext() {
        return gameContext;
    }

    public Entity getPlayer() {
        return player;
    }
}
