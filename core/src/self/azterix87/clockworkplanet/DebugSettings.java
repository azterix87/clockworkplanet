/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/8/17 5:58 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet;

public class DebugSettings {
    public static final boolean DEBUG_ENABLED = true;
    public static final boolean BYPASS_MAIN_MENU = false;
}
