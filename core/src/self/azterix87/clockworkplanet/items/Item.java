package self.azterix87.clockworkplanet.items;

import com.badlogic.gdx.math.Vector2;

public class Item {
    private String texture;
    private Vector2 spriteOrigin;
    private String attackAnimation;
    private String getUseAnimation;



    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public String getAttackAnimation() {
        return attackAnimation;
    }

    public void setAttackAnimation(String attackAnimation) {
        this.attackAnimation = attackAnimation;
    }
}
