package self.azterix87.clockworkplanet.items;

import self.azterix87.clockworkplanet.PlayerController;

public class WeaponSword extends MeleeWeapon {

    public WeaponSword(String texture) {
        this.setTexture(texture);
    }

    @Override
    protected float getCooldownTime(PlayerController playerController) {
        return 0.5F;
    }

    @Override
    public float getDamage(PlayerController playerController) {
        return 10;
    }

    @Override
    public float getAttackDistance(PlayerController playerController) {
        return 0.75F;
    }

    @Override
    public float getKnockback(PlayerController playerController) {
        return 8;
    }


}
