package self.azterix87.clockworkplanet.items;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import self.azterix87.clockworkplanet.PlayerController;
import self.azterix87.seraphim.Box2DWorld;
import self.azterix87.seraphim.components.CLivingStats;
import self.azterix87.seraphim.components.CTransform;
import self.azterix87.seraphim.systems.box2d.CBox2DBody;

public abstract class MeleeWeapon extends Weapon {
    public static final int knockbackMultiplier = 50;

    public MeleeWeapon() {

    }

    @Override
    protected void performAttack(Vector2 direction, PlayerController playerController) {
        CTransform transform = CTransform.mapper.get(playerController.getPlayer());
        Vector2 playerPos = new Vector2(transform.localPosition.x, transform.localPosition.y);
        Box2DWorld world = (Box2DWorld) playerController.getGameContext().getGameWorld();

        Entity entity = world.raytraceClosest(playerPos, direction.cpy().setLength(getAttackDistance(playerController)));

        if (entity == null) {
            return;
        }

        //KnockBack
        CBox2DBody bodyComponent = CBox2DBody.mapper.get(entity);
        Body body = bodyComponent.getBody();
        Vector2 knockback = direction.cpy().setLength(getKnockback(playerController) * knockbackMultiplier);
        body.applyLinearImpulse(knockback, new Vector2(), true);

        //Damage
        if (CLivingStats.mapper.has(entity)) {
            CLivingStats.mapper.get(entity).damage(getDamage(playerController));
        }
    }

    public abstract float getDamage(PlayerController playerController);

    public abstract float getAttackDistance(PlayerController playerController);

    public abstract float getKnockback(PlayerController playerController);

}
