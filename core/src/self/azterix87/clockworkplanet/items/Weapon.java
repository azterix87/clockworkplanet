package self.azterix87.clockworkplanet.items;

import com.badlogic.gdx.math.Vector2;
import self.azterix87.clockworkplanet.PlayerController;

public abstract class Weapon extends Item {
    private float cooldownCounter;

    public Weapon() {

    }

    public void update(float dt) {
        if (cooldownCounter > 0) {
            cooldownCounter -= dt;
        } else {
            cooldownCounter = 0;
        }
    }

    public final void attack(Vector2 direction, PlayerController playerController) {
        if (isReady()) {
            performAttack(direction, playerController);
            cooldownCounter = getCooldownTime(playerController);
        }
    }

    protected abstract void performAttack(Vector2 direction, PlayerController playerController);

    protected abstract float getCooldownTime(PlayerController playerController);

    protected boolean isReady() {
        return cooldownCounter <= 0;
    }
}
