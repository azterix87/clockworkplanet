/*
 * Project: ClockWorkPlanet
 * Author: azterix87
 * Last modified: 12/9/17 9:15 PM
 * Copyright (c) Azterix87  2017
 */

package self.azterix87.clockworkplanet;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import self.azterix87.clockworkplanet.ui.GameScreen;
import self.azterix87.clockworkplanet.ui.LoadingScreen;
import self.azterix87.clockworkplanet.ui.MainMenuScreen;
import self.azterix87.seraphim.GameContext;
import self.azterix87.seraphim.ScreenManager;
import self.azterix87.seraphim.assets.AssetCrawler;
import self.azterix87.seraphim.assets.AssetType;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimation;
import self.azterix87.seraphim.systems.motionanimation.MotionAnimationLoader;
import self.azterix87.seraphim.systems.spriterendering.AnimationLoader;

public class ClockworkPlanet extends Game {
    public static final String ASSETS_PATH = "assets";
    private GameContext gameContext;
    private ScreenManager screenManager;
    private AssetManager guiAssets;
    private ClockworkWorld world;


    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        guiAssets = new AssetManager(new LocalFileHandleResolver());

        Gdx.app.log("@ClockWorkPlanet", "Creating screens..");
        screenManager = new ScreenManager(this);

        Gdx.app.log("@ClockWorkPlanet", "Creating game context");
        gameContext = new GameContext();
        createScreens();
        Gdx.app.log("@ClockWorkPlanet", "Loading assets..");
        screenManager.setScreen("loading", false);
        loadGameAssets();
    }

    public void loadingFinished() {
        Gdx.app.log("@ClockWorkPlanet", "Loading finished, Initializing assets");
        initAssets();
        Gdx.app.log("@ClockWorkPlanet", "Asset initialization finished");
        world = new ClockworkWorld(gameContext);
        world.init();
        gameContext.setGameWorld(world);
        screenManager.setScreen("main_menu");
    }

    public void play() {
        Gdx.app.log("@ClockWorkPlanet", "Entering the game");
        world.startNewGame();
        screenManager.setScreen("game");
    }

    private void initAssets() {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 15;
        parameter.borderWidth =2;
        parameter.borderColor = Color.BLACK;
        parameter.magFilter = Texture.TextureFilter.Nearest;
        gameContext.assets.fontManager.generateAndRegisterFont("regular", "assets/fonts/a_AvanteBs.ttf", parameter);
    }

    private void loadGameAssets() {
        AssetCrawler assetCrawler = new AssetCrawler();

        //Register asset types
        assetCrawler.registerAssetType(new AssetType("Texture", Texture.class, ".jpg", ".png"));
        assetCrawler.registerAssetType(new AssetType("Texture Atlas", TextureAtlas.class, ".atlas"));
        assetCrawler.registerAssetType(new AssetType("Animation", Animation.class,
                new AnimationLoader(new LocalFileHandleResolver()), ".animation"));
        assetCrawler.registerAssetType(new AssetType("Motion animation", MotionAnimation.class,
                new MotionAnimationLoader(new LocalFileHandleResolver()), ".manim"));
        assetCrawler.registerAssetType(new AssetType("Font generator", FreeTypeFontGenerator.class,
                new FreeTypeFontGeneratorLoader(new LocalFileHandleResolver()), ".ttf"));

        assetCrawler.registerAssetType(new AssetType("Tiled Map", TiledMap.class,
                new TmxMapLoader(new LocalFileHandleResolver()), ".tmx"));

        //Load assets
        AssetManager assetManager = gameContext.assets.assetManager;
        assetCrawler.loadAssets(new FileHandle(ASSETS_PATH), gameContext.assets.assetManager);

        Gdx.app.log("@ClockWorkPlanet", "Loading assets..");
        LoadingScreen loadingScreen = (LoadingScreen) screenManager.getScreen("loading");
        loadingScreen.setOnUpdate(() -> {
            if (!assetManager.update()) {
                loadingScreen.setProgress(assetManager.getProgress());
            } else {
                loadingScreen.setOnUpdate(null);
                loadingFinished();
            }
        });
    }

    private void createScreens() {
        Skin skin = loadGUISkin();

        MainMenuScreen mainMenuScreen = new MainMenuScreen(skin);
        mainMenuScreen.setOnPlay(this::play);
        screenManager.addScreen("main_menu", mainMenuScreen);
        screenManager.addScreen("loading", new LoadingScreen(skin));
        screenManager.addScreen("game", new GameScreen(gameContext));
    }

    private Skin loadGUISkin() {
        Gdx.app.log("@ClockWorkPlanet", "loading GUI skin");

        String skinPath = "assets/skins/default/uiskin.json";
        guiAssets.load(skinPath, Skin.class);
        guiAssets.finishLoading();
        return guiAssets.get(skinPath);
    }

    @Override
    public void dispose() {
        Gdx.app.log("@ClockWorkPlanet", "Exiting..");
        gameContext.dispose();
        guiAssets.dispose();
    }
}
